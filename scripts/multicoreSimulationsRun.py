import multiprocessing as mp
from numericPearson import CorrelationTests
from pearsonComparison import PulseSynthesis
import matplotlib.pyplot as plt
import numpy as np
import time
import datetime

class SimulationRunMulticore(object):

    signal = PulseSynthesis()

    def __init__(self):
        pass
    
    def runMultiCore(self,
            patternPulseLen = 64,
            pulseType = signal.pmtPulse,
            nPulsesInTrace = 100,
            leadingToLenRatio = 0.45,
            simulationThresholdRange = (0.1, 1.2),
            simulationThresholdStep = 0.05,
            simulationSnrRange = (2.0, 10.0),
            simulationSnrStep = 0.5,
            pileUpBeta = 0,
            nCores = 4,
            plot3D = False,
            plotPatternPulse = True,
            outputFile = None):
        

        distanceBetweenPulses = [patternPulseLen * 0.25 , patternPulseLen * 10]
        
        # Ideal pulse used to generate coefficients
        c = self.signal.pmtPulse(
                length = patternPulseLen,
                amplitude = 1,
                noiseSigma = 0, #Ideal pulse means NO NOISE
                leadingProportion = leadingToLenRatio
            )
        
        if plotPatternPulse:
            #Plotting pattern pulse
            t = np.arange(len(c))
            plt.plot(t, c, marker = '.', linewidth = 0.75, label = 'Pattern Pulse', color = 'red')
            plt.legend(["Synthetic pattern pulse"])
            plt.show()


        # Data iterators 
        snrIterator = np.arange(simulationSnrRange[0],
                                simulationSnrRange[1] + simulationSnrStep,
                                simulationSnrStep
                                )
        snrIterator = np.around(snrIterator, 2)
        
        
        thrIterator = np.arange(simulationThresholdRange[0],
                                simulationThresholdRange[1] + simulationThresholdStep,
                                simulationThresholdStep)
        thrIterator = np.around(thrIterator, 2)
        
        
        
        # Multiprocessing queues to store split data after processing
        mpQueues = [mp.Queue() for i in range(nCores)]

        mpIterators = []
        print("Global iterator: " + str(snrIterator))

        # Splitting iterators to assign jobs to multiple cores
        mpIterators = np.array_split(snrIterator, nCores)

        print("Individual iterators: " + str(mpIterators))

        instances = []
        results = []

        startTime = time.time()

        # Running multiple instances of the simulator in parallel processes 
        for i in range(nCores):
            instances.append(
                CorrelationTests(
                    thresholdLimits = simulationThresholdRange,
                    thresholdStepSize = simulationThresholdStep,
                    snrLimits = [min(mpIterators[i]), max(mpIterators[i])],
                    snrStepSize = simulationSnrStep,
                    nPulses = nPulsesInTrace
                )
            )

            p = mp.Process(
                    target = instances[i].testsMixedSweeping, 
                    args = (
                        c, patternPulseLen, distanceBetweenPulses[0],
                        distanceBetweenPulses[1], pulseType, False, True, 
                        mpQueues[i], pileUpBeta
                    )
                )

            p.start()

            print("Processing range: " + str(mpIterators[i]))

        # Gathering processed data
        for j in range(nCores):
            results.append(mpQueues[j].get())
            p.join()
        
        
        # Rearranging results in single data frame
        ct = CorrelationTests(nPulses = nPulsesInTrace) # Using auxiliary definitions (KEYWORDS), not for computation
        
        resultsElements = (
            ct.KEYWORDS_CLASSIC,
            ct.KEYWORDS_MODIFIED,
            ct.KEYWORDS_SIGNAL,
            ct.KEYWORDS_SENSITIVITY_CLASSIC,
            ct.KEYWORDS_SENSITIVITY_MODIFIED,
            ct.KEYWORDS_SENSITIVITY_SIGNAL,
            ct.KEYWORDS_PRECISION_CLASSIC,
            ct.KEYWORDS_PRECISION_MODIFIED,
            ct.KEYWORDS_PRECISION_SIGNAL,
            ct.KEYWORDS_RECALL_CLASSIC,
            ct.KEYWORDS_RECALL_MODIFIED,
            ct.KEYWORDS_RECALL_SIGNAL,
            ct.KEYWORDS_CSI_MODIFIED,
            ct.KEYWORDS_CSI_SIGNAL
        )
        
        # Final data structure
        jointData = {}
        a = [[]] * len(snrIterator)
        b = [a] * len(thrIterator)
        
        
        for i in resultsElements:
            jointData[i] = b.copy()
            
        jointData[ct.KEYWORDS_SNR_ITERATOR] = snrIterator
        jointData[ct.KEYWORDS_THRESHOLD_ITERATOR] = results[0][ct.KEYWORDS_THRESHOLD_ITERATOR]
        
        classics = []
        modified = []
        signals = []
        sensitivitiesClassic = []
        sensitivitiesModified = []
        sensitivitiesSignal = []
        precisionsClassic = []
        precisionsModified = []
        precisionsSignal = []
        recallsClassic = []
        recallsModified = []
        recallsSignal = []
        csiClassic = []
        csiModified = []
        csiSignal = []
    

        # Joining previously splitted data into the standard data structure
        for thisDict in results:
            classics.append(thisDict[ct.KEYWORDS_CLASSIC])
            modified.append(thisDict[ct.KEYWORDS_MODIFIED])
            signals.append(thisDict[ct.KEYWORDS_SIGNAL])
            sensitivitiesClassic.append(thisDict[ct.KEYWORDS_SENSITIVITY_CLASSIC])
            sensitivitiesModified.append(thisDict[ct.KEYWORDS_SENSITIVITY_MODIFIED])
            sensitivitiesSignal.append(thisDict[ct.KEYWORDS_SENSITIVITY_SIGNAL])
            precisionsClassic.append(thisDict[ct.KEYWORDS_PRECISION_CLASSIC])
            precisionsModified.append(thisDict[ct.KEYWORDS_PRECISION_MODIFIED])
            precisionsSignal.append(thisDict[ct.KEYWORDS_PRECISION_SIGNAL])
            recallsClassic.append(thisDict[ct.KEYWORDS_RECALL_CLASSIC])
            recallsModified.append(thisDict[ct.KEYWORDS_RECALL_MODIFIED])
            recallsSignal.append(thisDict[ct.KEYWORDS_RECALL_SIGNAL])
            csiClassic.append(thisDict[ct.KEYWORDS_CSI_CLASSIC])
            csiModified.append(thisDict[ct.KEYWORDS_CSI_MODIFIED])
            csiSignal.append(thisDict[ct.KEYWORDS_CSI_SIGNAL])
            
        newClassics = []
        newModified = []
        newSignals = []
        newSensitivitiesClassic = []
        newSensitivitiesModified = []
        newSensitivitiesSignal = []
        newPrecisionsClassic = []
        newPrecisionsModified = []
        newPrecisionsSignal = []
        newRecallsClassic = []
        newRecallsModified = []
        newRecallsSignal = []
        newCsiClassic = []
        newCsiModified = []
        newCsiSignal = []
        
        # Changing array structures
        for i in range(len(classics[0])):
            newClassics.append([])
            newModified.append([])
            newSignals.append([])
            newSensitivitiesClassic.append([])
            newSensitivitiesModified.append([])
            newSensitivitiesSignal.append([])
            newPrecisionsClassic.append([])
            newPrecisionsModified.append([])
            newPrecisionsSignal.append([])
            newRecallsClassic.append([])
            newRecallsModified.append([])
            newRecallsSignal.append([])
            newCsiClassic.append([])
            newCsiModified.append([])
            newCsiSignal.append([])
            
            
            
            thisClassics = []
            thisModified = []
            thisSignals = []
            thisSensitivitiesClassic = []
            thisSensitivitiesModified = []
            thisSensitivitiesSignal = []
            thisPrecisionsClassic = []
            thisPrecisionsModified = []
            thisPrecisionsSignal = []
            thisRecallsClassic = []
            thisRecallsModified = []
            thisRecallsSignal = []
            thisCsiClassic = []
            thisCsiModified = []
            thisCsiSignal = []
            
            
            for j in range(len(classics)):
                thisClassics.extend(classics[j][i])
                thisModified.extend(modified[j][i])
                thisSignals.extend(signals[j][i])
                thisSensitivitiesClassic.extend(sensitivitiesClassic[j][i])
                thisSensitivitiesModified.extend(sensitivitiesModified[j][i])
                thisSensitivitiesSignal.extend(sensitivitiesSignal[j][i])
                thisPrecisionsClassic.extend(precisionsClassic[j][i])
                thisPrecisionsModified.extend(precisionsModified[j][i])
                thisPrecisionsSignal.extend(precisionsSignal[j][i])
                thisRecallsClassic.extend(recallsClassic[j][i])
                thisRecallsModified.extend(recallsModified[j][i])
                thisRecallsSignal.extend(recallsSignal[j][i])
                thisCsiClassic.extend(csiClassic[j][i])
                thisCsiModified.extend(csiModified[j][i])
                thisCsiSignal.extend(csiSignal[j][i])
            
            newClassics[i].extend(thisClassics)
            newModified[i].extend(thisModified)
            newSignals[i].extend(thisSignals)
            newSensitivitiesClassic[i].extend(thisSensitivitiesClassic)
            newSensitivitiesModified[i].extend(thisSensitivitiesModified)
            newSensitivitiesSignal[i].extend(thisSensitivitiesSignal)
            newPrecisionsClassic[i].extend(thisPrecisionsClassic)
            newPrecisionsModified[i].extend(thisPrecisionsModified)
            newPrecisionsSignal[i].extend(thisPrecisionsSignal)
            newRecallsClassic[i].extend(thisRecallsClassic)
            newRecallsModified[i].extend(thisRecallsModified)
            newRecallsSignal[i].extend(thisRecallsSignal)
            newCsiClassic[i].extend(thisCsiClassic)
            newCsiModified[i].extend(thisCsiModified)
            newCsiSignal[i].extend(thisCsiSignal)
            
        
        jointData[ct.KEYWORDS_CLASSIC] = newClassics
        jointData[ct.KEYWORDS_MODIFIED] = newModified
        jointData[ct.KEYWORDS_SIGNAL] = newSignals
        jointData[ct.KEYWORDS_SENSITIVITY_CLASSIC] = newSensitivitiesClassic
        jointData[ct.KEYWORDS_SENSITIVITY_MODIFIED] = newSensitivitiesModified
        jointData[ct.KEYWORDS_SENSITIVITY_SIGNAL] = newSensitivitiesSignal
        jointData[ct.KEYWORDS_PRECISION_CLASSIC] = newPrecisionsClassic
        jointData[ct.KEYWORDS_PRECISION_MODIFIED] = newPrecisionsModified
        jointData[ct.KEYWORDS_PRECISION_SIGNAL] = newPrecisionsSignal
        jointData[ct.KEYWORDS_RECALL_CLASSIC] = newRecallsClassic
        jointData[ct.KEYWORDS_RECALL_MODIFIED] = newRecallsModified
        jointData[ct.KEYWORDS_RECALL_SIGNAL] = newRecallsSignal
        jointData[ct.KEYWORDS_CSI_CLASSIC] = newCsiClassic
        jointData[ct.KEYWORDS_CSI_MODIFIED] = newCsiModified
        jointData[ct.KEYWORDS_CSI_SIGNAL] = newCsiSignal
        
        endTime = time.time()
        
        # Create compatible dataframe for storage
        df = ct.dataToDataFrame(
            triggersClassic = jointData[ct.KEYWORDS_CLASSIC],
            triggersModified = jointData[ct.KEYWORDS_MODIFIED],
            triggersSignal = jointData[ct.KEYWORDS_SIGNAL],
            snrIterator = jointData[ct.KEYWORDS_SNR_ITERATOR],
            thrIterator = jointData[ct.KEYWORDS_THRESHOLD_ITERATOR],
            
            sensitivityClassic = jointData[ct.KEYWORDS_SENSITIVITY_CLASSIC],
            sensitivityModified = jointData[ct.KEYWORDS_SENSITIVITY_MODIFIED],
            sensitivitySignal = jointData[ct.KEYWORDS_SENSITIVITY_SIGNAL],
            
            precisionClassic = jointData[ct.KEYWORDS_PRECISION_CLASSIC],
            precisionModified = jointData[ct.KEYWORDS_PRECISION_MODIFIED],
            precisionSignal = jointData[ct.KEYWORDS_PRECISION_SIGNAL],
            
            recallClassic = jointData[ct.KEYWORDS_RECALL_CLASSIC],
            recallModified = jointData[ct.KEYWORDS_RECALL_MODIFIED],
            recallSignal = jointData[ct.KEYWORDS_RECALL_SIGNAL],
            
            csiClassic = jointData[ct.KEYWORDS_CSI_CLASSIC],
            csiModified = jointData[ct.KEYWORDS_CSI_MODIFIED],
            csiSignal = jointData[ct.KEYWORDS_CSI_SIGNAL]
            
        )
        
        print(df)
        
        print("Time elapsed: " + str(datetime.timedelta(seconds = endTime - startTime)))
        
        
        
        if not outputFile:
            outputFile = './pulseType' + str(pulseType.__name__) + \
            '_pulseLen' + str(patternPulseLen) + \
            '_nPulses' + str(nPulsesInTrace) + \
            '_thrS' + str(simulationThresholdRange[0]) + \
            '_thrE' + str(simulationThresholdRange[1]) + \
            '_snrS' + str(simulationSnrRange[0]) + \
            '_snrE' + str(simulationSnrRange[1]) + \
            '_pileUp' + str(pileUpBeta) + \
            '.csv'
            
            
                   
        df.to_csv(outputFile)
        
        if plot3D:
            
            # Plot results
            # ct.plotResults3D(
            #     triggersClassic = jointData[ct.KEYWORDS_CLASSIC],
            #     triggersModified = jointData[ct.KEYWORDS_MODIFIED],
            #     triggersSignal = jointData[ct.KEYWORDS_SIGNAL],
            #     snrIterator = jointData[ct.KEYWORDS_SNR_ITERATOR],
            #     thrIterator = jointData[ct.KEYWORDS_THRESHOLD_ITERATOR]
            # )      
            
            ct.plotResults3D(
                snrIterator = jointData[ct.KEYWORDS_SNR_ITERATOR],
                thrIterator = jointData[ct.KEYWORDS_THRESHOLD_ITERATOR],
                triggersClassic = jointData[ct.KEYWORDS_CSI_CLASSIC],
                triggersModified = jointData[ct.KEYWORDS_CSI_MODIFIED],
                triggersSignal = jointData[ct.KEYWORDS_CSI_SIGNAL]
            )    



