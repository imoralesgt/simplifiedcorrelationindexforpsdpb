# Simplified correlation index for pulse-shape pattern recognition

## Numerical simulation

An automated Python script contains simulation parameters to be configured by the user. By executing this script, the simulation is started. Basic Python language knowledge on how to set variables and distinguish code from comments is required. The code is designed to run on Linux-based systems.

## Dependencies

In order to execute the script, the following dependencies must be previously installed:

- [Python 3.8+](https://www.python.org/downloads/)
- [Python Package Index](https://pypi.org/)
- [Matplotlib](https://pypi.org/project/matplotlib/)
- [Seaborn](https://pypi.org/project/seaborn/)
- [NumPy](https://pypi.org/project/numpy/)
- [SciPy](https://pypi.org/project/scipy/)
- [Pandas](https://pypi.org/project/pandas/)
- [FuncTools](https://pypi.org/project/functools/)
- [Statistics](https://pypi.org/project/statistics/)
- [iSort](https://pypi.org/project/isort/)
- [LabelLines](https://pypi.org/project/matplotlib-label-lines/)

## Configuring the simulation

Open in a text editor the simulation script:

    scripts/runPearson.py

Find the following text near the file header:

    ================================
    SIMULATION PARAMETERS START HERE
    ================================

Tune the parameters according to your requirements. Each parameter is defined as a Python variable, clearly described as a comment in the code.

**Do not modify anything below the parameters setup area**, signaled as follows:

    ==============================
    SIMULATION PARAMETERS END HERE
    ==============================

## Simulation execution

In console, navigate to the root folder of this project folder and execute the simulation by typing

    python3 ./scripts/runPearson.py

A comma-separated text file will be generated with the results, using the name convention defined in the parameters.
