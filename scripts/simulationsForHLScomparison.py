from pearsonComparison import PulseSynthesis
from numericPearson import CorrelationTests, CorrelationComparison
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os

if __name__ == '__main__':
    
    signal = PulseSynthesis()
    
    
    
    '''
    ======================================================================
    USER PARAMETERS: Modify these values to change the simulation behavior
    ======================================================================
    '''
    PULSE_LEN = 64
    PULSE_TYPE = signal.pmtPulse
    SIGNAL_TO_NOISE_RATIO = 4
    THRESHOLD_SIGMAS = int(0.5*SIGNAL_TO_NOISE_RATIO)
    ORIGINAL_PULSE_REPLICAS = 10
    MAX_TRACE_LEN = 1024 #Set to 0 (or False) to avoid truncating the trace
    # MAX_TRACE_LEN = False
    LEADING_TO_LENGTH_RATIO = 0.25
    DISTANCE_BETWEEN_PULSES = [PULSE_LEN * 0.0, PULSE_LEN * 3.0]
    LOW_FREQ_NOISE_INDEX = 0.0
    QUANTIZATION_BITS = False #Use 0 for no quantization
    OUTPUT_FILES_DIRECTORY = './simulationResultsHLS'
    DECIMAL_PLACES = 11

    
    '''
    ======================================================================
    USER PARAMETERS: Modify these values to change the simulation behavior
    ======================================================================
    '''
    
    os.makedirs(OUTPUT_FILES_DIRECTORY, exist_ok=True)
    OUTPUT_FILES_DIRECTORY = str(OUTPUT_FILES_DIRECTORY).rstrip('/') + '/'
    FILENAME_COEFFICIENTS = 'coefficients'
    FILENAME_CORRELATIONS = 'correlations'
    
    
    FILENAME_COEFFICIENTS = OUTPUT_FILES_DIRECTORY + FILENAME_COEFFICIENTS
    FILENAME_COEFFICIENTS += '_CoeffLen' + str(PULSE_LEN) + \
                             '_SNR' + str(SIGNAL_TO_NOISE_RATIO) + \
                             '_Quantization' + str(QUANTIZATION_BITS) + \
                             '_' + str(PULSE_TYPE.__name__) + \
                             '.csv'
    
    
    FILENAME_CORRELATIONS = OUTPUT_FILES_DIRECTORY + FILENAME_CORRELATIONS
    FILENAME_CORRELATIONS += '_CoeffLen' + str(PULSE_LEN) + \
                             '_SNR' + str(SIGNAL_TO_NOISE_RATIO) + \
                             '_Quantization' + str(QUANTIZATION_BITS) + \
                             '_' + str(PULSE_TYPE.__name__) + \
                             '.csv'
    
    # Ideal pulse generation: coefficients
    c = PULSE_TYPE(
        length = PULSE_LEN,
        amplitude = 1,
        noiseSigma = 0, #Ideal pulse => no noise
        leadingProportion = LEADING_TO_LENGTH_RATIO,
        quantizationBits = QUANTIZATION_BITS
    )
    
    # Noise reference MUST be always the same value: 1
    NOISE_AMPLITUDE = 1 # Setting noise sigma to 1
    MAX_PULSE_AMPLITUDE = signal.snrToSignalAmp(SIGNAL_TO_NOISE_RATIO, c, NOISE_AMPLITUDE)
    MIN_PULSE_AMPLITUDE = MAX_PULSE_AMPLITUDE # Every pulse amplitude is the same
    
    #Visual line value to show "valid" correlation threshold
    CORRELATION_THRESHOLD_LINE = (1 / SIGNAL_TO_NOISE_RATIO) * THRESHOLD_SIGMAS
    
    
    
    # Noisy synthetic signal
    x, xPos = signal.pmtTrace(
        nPulses = ORIGINAL_PULSE_REPLICAS,
        pulseLength = PULSE_LEN,
        pulseType = PULSE_TYPE,
        leadingProportion = LEADING_TO_LENGTH_RATIO,
        minAmp = MIN_PULSE_AMPLITUDE,
        maxAmp = MAX_PULSE_AMPLITUDE,
        traceNoiseSigma = NOISE_AMPLITUDE,
        lowFreqNoiseContribution = LOW_FREQ_NOISE_INDEX,
        minSeparation = DISTANCE_BETWEEN_PULSES[0],
        maxSeparation = DISTANCE_BETWEEN_PULSES[1],
        returnPulseLocations = True,
        plotNoiseDist = True,
        quantizationBits = QUANTIZATION_BITS
    )
    
    
    numPulses = len(xPos)
    
    # In case a limited number of samples is required for
    # the synthetic signal to run into the FPGA IP Cores
    if MAX_TRACE_LEN:
        x = x[:MAX_TRACE_LEN]
    
        i = 0
        while(i < numPulses):
            if xPos[i] >= MAX_TRACE_LEN:
                del xPos[i]
                numPulses -= 1
            i += 1
    
    
    
    '''
    ===================
    Original pulse plot
    ===================
    '''
    t = np.arange(len(c))

    plt.plot(t, c, label = "Synthetic pulse")
    plt.legend(["Original Pulse"])
    plt.show()
    
    
    '''
    =======================
    Coefficients generation
    =======================
    '''
    cc = CorrelationComparison()
    normalizedCoefficientsStd = cc.standardScoreStd(c)
    normalizedCoefficientsStd = cc.normalizeCoefficient(normalizedCoefficientsStd)
    
    normalizedCoefficientsAMD = cc.standardScoreAMD(c)
    normalizedCoefficientsAMD = cc.normalizeCoefficient(normalizedCoefficientsAMD)
    
    if DECIMAL_PLACES:
        normalizedCoefficientsStd = np.around(normalizedCoefficientsStd, DECIMAL_PLACES)
        normalizedCoefficientsAMD = np.around(normalizedCoefficientsAMD, DECIMAL_PLACES)
    
    dfCoeff = pd.DataFrame({'Coefficients_STD' : normalizedCoefficientsStd, 
                            'Coefficients_AMD' : normalizedCoefficientsAMD})
    
    dfCoeff.plot(title = 'Normalized coefficients')
    
    plt.show()
    
    #Add reversed versions of coefficients to output data frame    
    dfCoeff['Coefficients_STD_Reversed'] = np.flip(normalizedCoefficientsStd)
    dfCoeff['Coefficients_AMD_Reversed'] = np.flip(normalizedCoefficientsAMD)
    
    dfCoeff.to_csv(FILENAME_COEFFICIENTS)
    
    
    '''
    ===============================
    Sliding window correlation test
    ===============================
    '''
    
    cTest = CorrelationTests()
    
    thisCorrelationTest = cTest.computeSingleCorrelations(x = x, c = c,
                                                          plot = True,
                                                          signalAmplitude = MAX_PULSE_AMPLITUDE,
                                                          correlationThresholdLine = CORRELATION_THRESHOLD_LINE,
                                                          decimalPlaces = DECIMAL_PLACES)
    
    signalNormalizedTo1 = thisCorrelationTest[cTest.KEYWORDS_SIGNAL]
    signalNormalizedTo1 = signalNormalizedTo1 / np.max(signalNormalizedTo1)
    if DECIMAL_PLACES:
        signalNormalizedTo1 = np.around(signalNormalizedTo1, DECIMAL_PLACES)
    keywordSignalNormalizedTo1 = cTest.KEYWORDS_SIGNAL + '_Normalized'  

    
    dfResults = pd.DataFrame(thisCorrelationTest)
    dfResults[keywordSignalNormalizedTo1] = signalNormalizedTo1
    
    
    dfResults.to_csv(FILENAME_CORRELATIONS)
    