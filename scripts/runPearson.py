#!/bin/python3
from multicoreSimulationsRun import SimulationRunMulticore as pearsonSimulation
from pearsonComparison import PulseSynthesis

signalType = PulseSynthesis()

'''
================================
SIMULATION PARAMETERS START HERE
================================
'''

# (PATTERN_PULSE_LEN) Pattern pulse length (samples). Default is 64 if not specified.
PATTERN_PULSE_LEN = 64

# (PULSE_TYPE) Signal type may be chosen from the list of available types:
# - pmtPulse
# - triangularPulse
# - squaredPulse
# - deltaPulse
# Default is pmtPulse if not specified.

PULSE_TYPE = signalType.pmtPulse

# (N_PULSES) Number of pulses within each trace. Default is 100 if not specified. Positive integer expected.
N_PULSES = 1000

# (LEADING_TO_LENGTH_RATIO) Rising edge vs falling edge pulse proportion. Default is 0.45 if not specified
LEADING_TO_LENGTH_RATIO = 0.45

# (THRESHOLD_RANGE) Threshold sweeping range. Default is (0.1, 1.2) if not specified. 2-element iterable object (tuple/list) expected.
THRESHOLD_RANGE = (0.1, 1.75)

# (THRESHOLD_STEP) Threshold sweeping step. Default is 0.05 if not specified. Positive float expected.
THRESHOLD_STEP = 0.025

# (SNR_RANGE) Signal-to-noise ratio (SNR) sweeping range. Default is (2, 10) if not specified. 2-element iterable object (tuple/list) expected.
SNR_RANGE = (1, 10)

# (SNR_STEP) Signal-to-noise ratio (SNR) sweeping step. Default is 0.5 if not specified. Positive float expected.
SNR_STEP = 0.25

# (PILE_UP_BETA) Pile-up beta. Defined as the average separation between pulses in a trace following a Poisson process
# Default is 0 (no pile-up) if not specified. Positive float expected.
PILE_UP_BETA = 5

# (CPU_CORES) Number of cores used for processing. Default is 4 if not specified.
# Positive integer expected. Depends on the number of cores available.
CPU_CORES = 4

# (PLOT_PATTERN_PULSE) Plot pattern pulse before starting simulation. Default is True if not specified. Boolean expected
PLOT_PATTERN_PULSE = False

# (PLOT_3D_CSI) Plot 3D CSI result matrix. Default is False if not specified.
PLOT_3D_CSI = False

# (FILENAME) Output file name. If not specified (or None), an inferred filename will be used based on simulation parameters
FILENAME = None


'''
==============================
SIMULATION PARAMETERS END HERE
==============================
'''

if __name__ == "__main__":
    mySimulation = pearsonSimulation()
    mySimulation.runMultiCore(
        patternPulseLen = PATTERN_PULSE_LEN,
        pulseType = PULSE_TYPE,
        nPulsesInTrace = N_PULSES,
        leadingToLenRatio = LEADING_TO_LENGTH_RATIO,
        simulationThresholdRange = THRESHOLD_RANGE,
        simulationSnrRange = SNR_RANGE,
        simulationThresholdStep = THRESHOLD_STEP,
        simulationSnrStep = SNR_STEP,
        pileUpBeta = PILE_UP_BETA,
        nCores = CPU_CORES,
        plotPatternPulse = PLOT_PATTERN_PULSE,
        plot3D = PLOT_3D_CSI,
        outputFile = FILENAME
    )
