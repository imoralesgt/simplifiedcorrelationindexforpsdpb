from pearsonComparison import PulseSynthesis
import numpy as np
import matplotlib.pyplot as plt
from time import time



'''
Pearson comparison for correlation between data and pattern pulse
'''

plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams["font.size"] = 12
class CorrelationComparison(object):
    def __init__(self):
        pass
        
    '''
    Pearson correlation using original standard deviation definition
    Gets standardized and normalized coefficients
    '''   
    def pearsonClassic(self, x, c, avoidSignalNormalization = False):
        
        # Coefficient normalization
        c = self.standardScoreStd(c)
        c = self.normalizeCoefficient(c)
        
        # Data normalization
        if not avoidSignalNormalization:
            x = self.standardScoreStd(x)
        
        # Scalar product -> correlation        
        r = np.dot(x, c)
        
        return r
    
        
    '''
    Simple pearson-like correlation using average mean deviation
    Gets standardized and normalized coefficients
    '''
    def pearsonModified(self, x, c, avoidSignalNormalization = False):
        # Coefficient normalization
        c = self.standardScoreAMD(c)
        c = self.normalizeCoefficient(c)
        
        # Data normalization
        if not avoidSignalNormalization:
            x = self.standardScoreAMD(x)
        
        # Scalar product -> correlation        
        r = np.dot(x, c)
        
        return r
    
    
    '''
    Runs a sliding window to correlate a "continuous" signal with a constant
    set of samples (coefficients). Expects a signal (x) equal or longer than
    the coefficients vector itself. Also, expects a list with the characteristic
    signal (no normalization required). The regression type/algorithm must be
    specified (by calling a method within this class).

    Returns a list with the correlation within the sliding window
    '''
    def correlate(self, x, c, algorithm, avoidSignalNormalization = False):
        
        lenSignal = len(x)
        lenCoeff  = len(c)

        #Coefficients must be shorter or equal to the signal itself
        if lenCoeff > lenSignal:
            return None

        
        if algorithm == self.pearsonClassic:
            # Coefficient standardization
            c = self.standardScoreStd(c)
            

        elif algorithm == self.pearsonModified:
            # Coefficient standardization
            c = self.standardScoreAMD(c)
            

        # In case a wrong algorithm has been chosen
        else:
            return None


        # Coefficient normalization
        c = self.normalizeCoefficient(c)

        # Results will be stored here
        correlations = []

        for i in range(lenSignal - lenCoeff + 1):
            windowedX = x[i:i + lenCoeff]
            correlations.append(algorithm(windowedX, c, avoidSignalNormalization = avoidSignalNormalization))
        
        return np.array(correlations)

        
    
    
    
    '''
    =================
    Auxiliary methods
    =================
    '''
    
    
    ''' 
    Compute standard score based on standard deviation
    '''
    def standardScoreStd(self, x):
        if type(x) == list:
            x = np.array(x)
        
        avg = np.average(x)
        std = np.std(x)

        return (x - avg) / std
    
    
    ''' 
    Compute standard score based on absolute mean deviation
    '''  
    def standardScoreAMD(self, x):
        if type(x) == list:
            x = np.array(x)
        
        avg = np.average(x)
        amd = np.average([np.abs(i - avg) for i in x])
        
        return (x - avg) / amd
        
    
    '''
    Coefficient normalization based on vector magnitude (norm)
    '''
    def normalizeCoefficient(self, c):
        if type(c) == list:
            c = np.array(c)
            
        return c / np.dot(c, c)
    


'''
Runs a set of predefined tests to compare the triggering performance
of each algorithm, checking with different SNR and Threshold values
'''
class CorrelationTests(object):
        
        # Expecting tests parameters
        def __init__(self,
                     thresholdLimits = [0.01, 1.5],
                     thresholdStepSize = 0.005,
                     snrLimits = [-10.0, 10.0], 
                     snrStepSize = 0.5,
                     nPulses = 100
                     ):
            self.cc = CorrelationComparison()
            self.signal = PulseSynthesis()
            
            self.__thresholdLimits = thresholdLimits
            self.__thresholdStepSize = thresholdStepSize
            self.__snrLimits = snrLimits
            self.__snrStepSize = snrStepSize      
            self.__nPulses = nPulses
            
            self.KEYWORDS_CLASSIC = "Classic"
            self.KEYWORDS_MODIFIED = "Modified"
            self.KEYWORDS_SIGNAL = "Signal"
            self.KEYWORDS_SNR_ITERATOR = "SNR_Iterator"
            self.KEYWORDS_THRESHOLD_ITERATOR = "Threshold_Iterator"
            self.KEYWORDS_TRUE_POSITIVES = "TP"
            self.KEYWORDS_FALSE_POSITIVES = "FP"
            self.KEYWORDS_FALSE_NEGATIVES = "FN"
            self.KEYWORDS_TRUE_NEGATIVES = "TN"
            self.KEYWORDS_SENSITIVITY_CLASSIC = "Sensitivity_Classic"
            self.KEYWORDS_SENSITIVITY_MODIFIED = "Sensitivity_Modified"
            self.KEYWORDS_SENSITIVITY_SIGNAL = "Sensitivity_Signal"
            self.KEYWORDS_PRECISION_CLASSIC = "Precision_Classic"
            self.KEYWORDS_PRECISION_MODIFIED = "Precision_Modified"
            self.KEYWORDS_PRECISION_SIGNAL = "Precision_Signal"
            self.KEYWORDS_RECALL_CLASSIC = "Recall_Classic"
            self.KEYWORDS_RECALL_MODIFIED = "Recall_Modified"
            self.KEYWORDS_RECALL_SIGNAL  = "Recall_Signal"
            self.KEYWORDS_FPR_CLASSIC = "FPR_Classic"
            self.KEYWORDS_FPR_MODIFIED = "FPR_Modified"
            self.KEYWORDS_FPR_SIGNAL = "FPR_Signal"
            self.KEYWORDS_CSI_CLASSIC = "CSI_Classic"
            self.KEYWORDS_CSI_MODIFIED = "CSI_Modified"
            self.KEYWORDS_CSI_SIGNAL = "CSI_Signal"
            self.POLARITY_POSITIVE = 1
            self.POLARITY_NEGATIVE = -1
        
        # Synthesize a signal trace based on its expected parameters
        # Simply use cc.pmtTrace to generate the expected signal and cc.snrToSigma to compute noise sigma
        
        
        
        
        
        # Compute a single correlation run with both algorithms 
        def computeSingleCorrelations(self, x, c, plot = False, signalAmplitude = 1,
                                      correlationThresholdLine = 0, decimalPlaces = False,
                                      avoidSignalNormalization = False, xLims = [], includeModified = True):
                      
            # Compute correlations (classic and modified algorithms)
            __pearsonClassic = self.cc.correlate(x, c, self.cc.pearsonClassic, avoidSignalNormalization = avoidSignalNormalization)
            if includeModified:
                __pearsonModified = self.cc.correlate(x, c, self.cc.pearsonModified, avoidSignalNormalization = avoidSignalNormalization)
            
            zeroesLenClassic = len(x) - len(__pearsonClassic)
            if includeModified:
                zeroesLenModified = len(x) - len(__pearsonModified)
            
            # Add leading zeroes to the correlation trace, to fit the original signal's length
            __pearsonClassic = np.append(__pearsonClassic, np.zeros(zeroesLenClassic))
            if includeModified:        
                __pearsonModified = np.append(__pearsonModified, np.zeros(zeroesLenModified))
            
            if type(decimalPlaces) == int and decimalPlaces > 0:
                __pearsonClassic = np.around(__pearsonClassic, decimalPlaces)
                if includeModified:
                    __pearsonModified = np.around(__pearsonModified, decimalPlaces)
                x = np.around(x, decimalPlaces)
            
            
            if plot:  
                # Original signal
                plt.figure(1)
                plt.subplot(211)
                plt.plot(x, '-', linewidth = 0.8, markeredgewidth = 0.1)
                # plt.legend(["Stimulus signal ($x$)"])
                plt.title("Stimulus signal ($x$)")
                plt.ylabel("Amplitude (a.u.)")
                plt.xlabel("Time (samples)")
                if len(xLims):
                    plt.xlim(xLims[0], xLims[1])


                # Second row: results
                plt.subplot(212)
                
                legendList = []

                
                xAxis = np.arange(len(__pearsonClassic))
                
                # Test correlation threshold                
                plotCorrelationThreshold = correlationThresholdLine
                if plotCorrelationThreshold:
                    legendList += ["Correlation threshold"]
                    plt.plot(xAxis, [correlationThresholdLine]*len(xAxis), 'm--' , linewidth = 0.75)
                        
                
                    
                # Comparison between classic correlation and modified corelation
                if includeModified:
                    plt.plot(xAxis, __pearsonClassic, __pearsonModified, linewidth = 0.75)    
                    legendList += ["Classic correlation", "Simplified correlation"]           
                else:
                    plt.plot(xAxis, __pearsonClassic, linewidth = 0.75, color = 'darkgreen')  
                    legendList += ["Correlation"]                        
                plt.axhline(y = 0, color = 'gray', linewidth = 0.4, linestyle = '--')
                plt.xlabel("Time (samples)")
                plt.ylabel("Correlation ($\\rho$)")
                              
                if len(xLims):
                    patternShift = int(len(c) / 2)
                    plt.xlim(xLims[0] - patternShift, xLims[1] - patternShift)
                
                if len(legendList) > 1:
                    plt.legend(legendList)
                plt.show()
           
            # Return a dictionary with the results and the original signal
            if includeModified:
                return {self.KEYWORDS_CLASSIC: __pearsonClassic, self.KEYWORDS_MODIFIED : __pearsonModified, self.KEYWORDS_SIGNAL: x}
            
            return {self.KEYWORDS_CLASSIC: __pearsonClassic, self.KEYWORDS_SIGNAL: x}
        
        
        
        
        
        # Compute (and plot) single correlation triggers using a signal with a defined SNR
        def testThresholdSweeping(self, signal, signalPulsePositions, pearsonClassic,
                                  pearsonModified, maxTraceAmplitude, plot = False, singlePulseLen = 0):
            
            expectedPulses = len(signalPulsePositions)
            
            thresholdLimits = self.getThresholdLimits()
            thresholdStepSize = self.getThresholdStepSize()
            
            thresholds = np.arange(thresholdLimits[0], thresholdLimits[1] + thresholdStepSize, thresholdStepSize)

            thisSignalAmp = maxTraceAmplitude
            
            # Length of correlation results
            t = np.arange(len(pearsonClassic))
            
            triggersClassic = []
            triggersModified = []
            triggersSignal = []
            
            sensitivitiesClassic = []
            sensitivitiesModified = []
            sensitivitiesSignal = []

            precisionsClassic = []
            precisionsModified = []
            precisionsSignal = []

            fprsClassic = []
            fprsModified = []
            fprsSignal = []
            
            csiClassic = []
            csiModified = []
            csiSignal = []
            
            for thisThreshold in thresholds:

                thisThreshold = round(thisThreshold, 4)
                
                print("Trigger: " + str(thisThreshold))
            
                # Determine triggers with classic algorithm
                thisTriggersList = self.findTriggersCrossLevel(pearsonClassic, thisThreshold, self.POLARITY_POSITIVE)
                thisTriggerCount = self.countTriggers(thisTriggersList)
                triggersClassic.append(thisTriggerCount)
                
                
                # Determine triggers with modified algorithm
                thisTriggersList = self.findTriggersCrossLevel(pearsonModified, thisThreshold, self.POLARITY_POSITIVE)
                thisTriggerCount = self.countTriggers(thisTriggersList)
                triggersModified.append(thisTriggerCount)
                
                
                # Determine triggers with original signal (expected to have arbitrary amplitude, but signal amplitude must be provided)
                thisTriggersList = self.findTriggersCrossLevel(signal, round(thisThreshold * thisSignalAmp, 4), self.POLARITY_POSITIVE)
                thisTriggerCount = self.countTriggers(thisTriggersList)
                triggersSignal.append(thisTriggerCount)
                
                
                
                
                # Determine sensitivities with classic algorithm
                thisSensitivitiesList = self.computeSensitivity(expectedPulsePositions = signalPulsePositions,
                                                                singlePulseLength = singlePulseLen,
                                                                signalUnderTest = pearsonClassic,
                                                                triggerLevel = thisThreshold,
                                                                polarity = self.POLARITY_POSITIVE,
                                                                isThisACorrelatedPulse = True)
                sensitivitiesClassic.append(thisSensitivitiesList)
                
                # Determine sensitivities with modified algorithm
                thisSensitivitiesList = self.computeSensitivity(expectedPulsePositions = signalPulsePositions,
                                                                singlePulseLength = singlePulseLen,
                                                                signalUnderTest = pearsonModified,
                                                                triggerLevel = thisThreshold,
                                                                polarity = self.POLARITY_POSITIVE,
                                                                isThisACorrelatedPulse = True)
                sensitivitiesModified.append(thisSensitivitiesList)
                
                # Determine sensitivities with original signal (expected to have arbitrary amplitude, but signal amplitude must be provided with threshold computation)
                thisSensitivitiesList = self.computeSensitivity(signalPulsePositions, singlePulseLen, signal, thisThreshold * thisSignalAmp, self.POLARITY_POSITIVE)
                sensitivitiesSignal.append(thisSensitivitiesList)
                
                




                # Determine precisions with classic algorithm
                thisPrecisionsList = self.computePrecision(expectedPulsePositions = signalPulsePositions,
                                                                singlePulseLength = singlePulseLen,
                                                                signalUnderTest = pearsonClassic,
                                                                triggerLevel = thisThreshold,
                                                                polarity = self.POLARITY_POSITIVE,
                                                                isThisACorrelatedPulse = True)
                precisionsClassic.append(thisPrecisionsList)
                
                # Determine precisions with modified algorithm
                thisPrecisionsList = self.computePrecision(expectedPulsePositions = signalPulsePositions,
                                                                singlePulseLength = singlePulseLen,
                                                                signalUnderTest = pearsonModified,
                                                                triggerLevel = thisThreshold,
                                                                polarity = self.POLARITY_POSITIVE,
                                                                isThisACorrelatedPulse = True)
                precisionsModified.append(thisPrecisionsList)
                
                # Determine precisions with original signal (expected to have arbitrary amplitude, but signal amplitude must be provided with threshold computation)
                thisPrecisionsList = self.computePrecision(signalPulsePositions, singlePulseLen, signal, thisThreshold * thisSignalAmp, self.POLARITY_POSITIVE)
                precisionsSignal.append(thisPrecisionsList)




                #Recall is the same as Sensitivity. Thus, recall won't be computed here again; instead, the Sensitivity 
                #results will be added as a Recall column into the results CSV file


                # Determine False Positive Rates with classic algorithm
                thisFPRsList = self.computeFalsePositiveRate(expectedPulsePositions = signalPulsePositions,
                                                                singlePulseLength = singlePulseLen,
                                                                signalUnderTest = pearsonClassic,
                                                                triggerLevel = thisThreshold,
                                                                polarity = self.POLARITY_POSITIVE,
                                                                isThisACorrelatedPulse = True)
                fprsClassic.append(thisFPRsList)

                # Determine False Positive Rates with modified algorithm
                thisFPRsList = self.computeFalsePositiveRate(expectedPulsePositions = signalPulsePositions,
                                                            singlePulseLength = singlePulseLen,
                                                            signalUnderTest = pearsonModified,
                                                            triggerLevel = thisThreshold,
                                                            polarity = self.POLARITY_POSITIVE,
                                                            isThisACorrelatedPulse = True)
                fprsModified.append(thisFPRsList)



                # Determine False Positive Rates with original signal (expected to have arbitrary amplitude, but signal amplitude must be provided with threshold computation)
                thisFPRsList = self.computeFalsePositiveRate(signalPulsePositions, singlePulseLen, signal, thisThreshold * thisSignalAmp, self.POLARITY_POSITIVE)
                fprsSignal.append(thisFPRsList)
                
                
                # Determine CSI with classic algorithm
                thisCsiList = self.computeCriticalSuccessIndex(expectedPulsePositions = signalPulsePositions,
                                                                singlePulseLength = singlePulseLen,
                                                                signalUnderTest = pearsonClassic,
                                                                triggerLevel = thisThreshold,
                                                                polarity = self.POLARITY_POSITIVE,
                                                                isThisACorrelatedPulse = True)
                csiClassic.append(thisCsiList)
                
                # Determine CSI with modified algorithm
                thisCsiList = self.computeCriticalSuccessIndex(expectedPulsePositions = signalPulsePositions,
                                                                singlePulseLength = singlePulseLen,
                                                                signalUnderTest = pearsonModified,
                                                                triggerLevel = thisThreshold,
                                                                polarity = self.POLARITY_POSITIVE,
                                                                isThisACorrelatedPulse = True)
                csiModified.append(thisCsiList)
                
                # Determine CSI with original signal (expected to have arbitrary amplitude, but signal amplitude must be provided with threshold computation)
                thisCsiList = self.computeCriticalSuccessIndex(signalPulsePositions, singlePulseLen, signal, thisThreshold * thisSignalAmp, self.POLARITY_POSITIVE)
                csiSignal.append(thisCsiList)
                
            
            if plot:
                # Plot absolute trigger rates
                plt.plot(thresholds, triggersClassic, thresholds, triggersModified, thresholds, triggersSignal, linewidth = 0.8)
                plt.plot(thresholds, [expectedPulses]*len(thresholds), 'r--', linewidth = 0.75)
                plt.title("Threshold sweeping with fixed SNR " + str(SIGNAL_TO_NOISE_RATIO))
                plt.xlabel("Trigger level")
                plt.ylabel("Trigger count")
                plt.legend(["Classic Triggers", "Modified Triggers", "Signal Triggers"])
                plt.show()    
                
                # Plot sensitivities
                plt.plot(thresholds, sensitivitiesClassic, thresholds, sensitivitiesModified, thresholds, sensitivitiesSignal, linewidth = 0.8)
                plt.plot(thresholds, [1]*len(thresholds), 'r--', linewidth = 0.75)
                plt.title("Threshold sweeping with fixed SNR " + str(SIGNAL_TO_NOISE_RATIO))
                plt.xlabel("Trigger level")
                plt.ylabel("Sensitivity")
                plt.legend(["Classic Triggers", "Modified Triggers", "Signal Triggers"])
                plt.show()  
                
                # Plot Critical Success Indexes
                plt.plot(thresholds, csiClassic, thresholds, csiModified, thresholds, csiSignal, linewidth = 0.8)
                plt.plot(thresholds, [1]*len(thresholds), 'r--', linewidth = 0.75)
                plt.title("Threshold sweeping with fixed SNR " + str(SIGNAL_TO_NOISE_RATIO))
                plt.xlabel("Trigger level")
                plt.ylabel("Critical Success Index")
                plt.legend(["Classic Triggers", "Modified Triggers", "Signal Triggers"])
                plt.show()  

                from sklearn.metrics import auc

                print("AUC Classic: " + str(auc(thresholds, csiClassic)))
                print("AUC Modified: " + str(auc(thresholds, csiModified)))
                print("AUC Signal: " + str(auc(thresholds, csiSignal)))

                # Precision-Recall plot. Remember: Recall is the same as Sensitivity
                plt.plot(sensitivitiesClassic, precisionsClassic,
                 sensitivitiesModified, precisionsModified,
                 sensitivitiesSignal, precisionsSignal, marker='o', linewidth = 0.8)
                plt.title("Precision-Recall plot for threshold sweeping with fixed SNR " + str(SIGNAL_TO_NOISE_RATIO))
                plt.xlabel("Recall")
                plt.ylabel("Precision")
                plt.legend(["Classic Triggers", "Modified Triggers", "Signal Triggers"])
                plt.show()

                # ROC Plot. True Positive Rate (recall/sensitivity) vs False Positive Rate
                plt.plot(fprsClassic, sensitivitiesClassic,
                 fprsModified, sensitivitiesModified,
                 fprsSignal, sensitivitiesSignal, linewidth = 0.8)
                plt.plot([0, 1], [0, 1], 'gray', '--', linewidth = 0.75)
                plt.title("ROC plot for threshold sweeping with fixed SNR " + str(SIGNAL_TO_NOISE_RATIO))
                plt.xlabel("False Positive Rate")
                plt.ylabel("True Positive Rate")
                plt.legend(["Classic Triggers", "Modified Triggers", "Signal Triggers"])
                plt.show()
                
                  

            return {self.KEYWORDS_CLASSIC: triggersClassic,
                    self.KEYWORDS_MODIFIED : triggersModified,
                    self.KEYWORDS_SIGNAL: triggersSignal,
                    self.KEYWORDS_SENSITIVITY_CLASSIC : sensitivitiesClassic,
                    self.KEYWORDS_SENSITIVITY_MODIFIED : sensitivitiesModified,
                    self.KEYWORDS_SENSITIVITY_SIGNAL : sensitivitiesSignal,
                    self.KEYWORDS_PRECISION_CLASSIC : precisionsClassic,
                    self.KEYWORDS_PRECISION_MODIFIED : precisionsModified,
                    self.KEYWORDS_PRECISION_SIGNAL : precisionsSignal,
                    self.KEYWORDS_RECALL_CLASSIC : sensitivitiesClassic,
                    self.KEYWORDS_RECALL_MODIFIED : sensitivitiesModified,
                    self.KEYWORDS_RECALL_SIGNAL : sensitivitiesSignal,
                    self.KEYWORDS_FPR_CLASSIC : fprsClassic,
                    self.KEYWORDS_FPR_MODIFIED : fprsModified,
                    self.KEYWORDS_FPR_SIGNAL : fprsSignal,
                    self.KEYWORDS_CSI_CLASSIC : csiClassic,
                    self.KEYWORDS_CSI_MODIFIED : csiModified,
                    self.KEYWORDS_CSI_SIGNAL : csiSignal
                    }
        
        
        # Compute (and plot) single correlation triggers by synthesizing signal with
        # differnet SNR values, but triggering on a static threshold
        def testsSnrSweeping(self, c, threshold, pulseLength, minPulseSeparation,
                             maxPulseSeparation, pulseType, plot = False, correlationThresholdLine = 0,
                             pileUp = False, pileUpBeta = 1.5):
            
            minSnr, maxSnr = self.getSnrLimits()
            snrStep = self.getSnrStepSize()
            
            snrIterator = np.arange(minSnr, maxSnr + snrStep, snrStep)
            snrIterator = np.around(snrIterator, 2)
            
            triggersClassic = []
            triggersModified = []
            triggersSignal = []
            
            sensitivitiesClassic = []
            sensitivitiesModified = []
            sensitivitiesSignal = []

            precisionsClassic = []
            precisionsModified = []
            precisionsSignal = []
            
            csiClassic = []
            csiModified = []
            csiSignal = []
            
            for snr in snrIterator:
                
                print("Processing SNR " + str(snr))
                
                thisNoiseAmp = 1 #Setting noise signal as 1
                thisTraceAmp = self.signal.snrToSignalAmp(snr)
                # thisTraceAmp = self.signal.snrToSignalAmp(snr = snr, patternPulse = c)
                
                if pileUp:
                    thisX, thisXPulseLocations = self.signal.pmtTraceWithPileup(
                        nPulses = self.getNPulses(),
                        pulseLength = pulseLength,
                        pulseType = pulseType,
                        normalizedBeta = pileUpBeta,
                        traceNoiseSigma = thisNoiseAmp,
                        minAmp = thisTraceAmp,
                        maxAmp = thisTraceAmp,
                        returnPulseLocations = True,
                        amplitudeFromSNR = snr
                    )
                
                else:
                    thisX, thisXPulseLocations = self.signal.pmtTrace(
                        nPulses = self.getNPulses(),
                        pulseLength = pulseLength,
                        # pulseType = self.signal.pmtPulse,
                        pulseType = pulseType,
                        minSeparation = minPulseSeparation,
                        maxSeparation = maxPulseSeparation,
                        traceNoiseSigma = thisNoiseAmp,
                        minAmp = thisTraceAmp,
                        maxAmp = thisTraceAmp,
                        returnPulseLocations = True,
                        amplitudeFromSNR = snr
                    )
                
                thisCorrelations = self.computeSingleCorrelations(thisX, c,
                                                                    plot = False,
                                                                    signalAmplitude = thisTraceAmp,
                                                                    correlationThresholdLine = correlationThresholdLine
                                                                  )
                
                thisClassic = thisCorrelations["Classic"]
                thisModified = thisCorrelations["Modified"]
                
                expectedTriggers = len(thisXPulseLocations)
                dataLen = len(thisClassic)
                
                threshold = round(threshold, 4)
                absoluteThreshold = round(thisTraceAmp * threshold, 4)

                # Determine triggers with classic algorithm
                triggerClassicList = self.findTriggersCrossLevel(thisClassic, threshold, self.POLARITY_POSITIVE)
                thisTriggerCount = self.countTriggers(triggerClassicList)
                triggersClassic.append(thisTriggerCount)                
                
                
                # Determine triggers with modified algorithm
                triggerModifiedList = self.findTriggersCrossLevel(thisModified, threshold, self.POLARITY_POSITIVE)
                thisTriggerCount = self.countTriggers(triggerModifiedList)
                triggersModified.append(thisTriggerCount)

                
                # Determine triggers with original signal
                triggerSignalList = self.findTriggersCrossLevel(thisX, absoluteThreshold, self.POLARITY_POSITIVE)
                thisTriggerCount = self.countTriggers(triggerSignalList)
                triggersSignal.append(thisTriggerCount)
                


                # Determine sensitivities with classic algorithm
                thisSensitivitiesList = self.computeSensitivity(expectedPulsePositions = thisXPulseLocations,
                                                                singlePulseLength = pulseLength,
                                                                signalUnderTest = thisClassic,
                                                                triggerLevel = threshold,
                                                                polarity = self.POLARITY_POSITIVE,
                                                                isThisACorrelatedPulse = True)
                sensitivitiesClassic.append(thisSensitivitiesList)
                
                # Determine sensitivities with modified algorithm
                thisSensitivitiesList = self.computeSensitivity(expectedPulsePositions = thisXPulseLocations,
                                                                singlePulseLength = pulseLength,
                                                                signalUnderTest = thisModified,
                                                                triggerLevel = threshold,
                                                                polarity = self.POLARITY_POSITIVE,
                                                                isThisACorrelatedPulse = True)
                sensitivitiesModified.append(thisSensitivitiesList)
                
                # Determine sensitivities with original signal (expected to have arbitrary amplitude, but signal amplitude must be provided with threshold computation)
                thisSensitivitiesList = self.computeSensitivity(thisXPulseLocations, pulseLength, thisX, absoluteThreshold, self.POLARITY_POSITIVE)
                sensitivitiesSignal.append(thisSensitivitiesList)
                
                



                # Determine precisions with classic algorithm
                thisPrecisionsList = self.computePrecision(expectedPulsePositions = thisXPulseLocations,
                                                                singlePulseLength = pulseLength,
                                                                signalUnderTest = thisClassic,
                                                                triggerLevel = threshold,
                                                                polarity = self.POLARITY_POSITIVE,
                                                                isThisACorrelatedPulse = True)
                precisionsClassic.append(thisPrecisionsList)
                
                # Determine precisions with modified algorithm
                thisPrecisionsList = self.computePrecision(expectedPulsePositions = thisXPulseLocations,
                                                                singlePulseLength = pulseLength,
                                                                signalUnderTest = thisModified,
                                                                triggerLevel = threshold,
                                                                polarity = self.POLARITY_POSITIVE,
                                                                isThisACorrelatedPulse = True)
                precisionsModified.append(thisPrecisionsList)
                
                # Determine precisions with original signal (expected to have arbitrary amplitude, but signal amplitude must be provided with threshold computation)
                thisPrecisionsList = self.computePrecision(thisXPulseLocations, pulseLength, thisX, absoluteThreshold, self.POLARITY_POSITIVE)
                precisionsSignal.append(thisPrecisionsList)


                
                # Determine CSI with classic algorithm
                thisCsiList = self.computeCriticalSuccessIndex(expectedPulsePositions = thisXPulseLocations,
                                                                singlePulseLength = pulseLength,
                                                                signalUnderTest = thisClassic,
                                                                triggerLevel = threshold,
                                                                polarity = self.POLARITY_POSITIVE,
                                                                isThisACorrelatedPulse = True)
                csiClassic.append(thisCsiList)
                
                # Determine CSI with modified algorithm
                thisCsiList = self.computeCriticalSuccessIndex(expectedPulsePositions = thisXPulseLocations,
                                                                singlePulseLength = pulseLength,
                                                                signalUnderTest = thisModified,
                                                                triggerLevel = threshold,
                                                                polarity = self.POLARITY_POSITIVE,
                                                                isThisACorrelatedPulse = True)
                csiModified.append(thisCsiList)
                
                # Determine CSI with original signal (expected to have arbitrary amplitude, but signal amplitude must be provided with threshold computation)
                thisCsiList = self.computeCriticalSuccessIndex(thisXPulseLocations, pulseLength, thisX, absoluteThreshold, self.POLARITY_POSITIVE)
                csiSignal.append(thisCsiList)
            
            
            
                
            if plot:
                # Plot absolute trigger rates
                plt.plot(snrIterator, triggersClassic, snrIterator, triggersModified, snrIterator, triggersSignal, linewidth = 0.8)
                plt.plot(snrIterator, [expectedTriggers]*len(snrIterator), 'r--', linewidth = 0.75)
                plt.title("SNR sweeping with fixed trigger at " + str(CORRELATION_THRESHOLD_LINE))
                plt.xlabel("Signal-to-Noise Ratio")
                plt.ylabel("Number of triggers")
                plt.legend(["Classic Triggers", "Modified Triggers", "Signal Triggers"])
                plt.show()
                
                # Plot sensitivities
                plt.plot(snrIterator, sensitivitiesClassic, snrIterator, sensitivitiesModified, snrIterator, sensitivitiesSignal, linewidth = 0.8)
                plt.plot(snrIterator, [1]*len(snrIterator), 'r--', linewidth = 0.75)
                plt.title("SNR sweeping with fixed trigger at " + str(CORRELATION_THRESHOLD_LINE))
                plt.xlabel("Signal-to-Noise Ratio")
                plt.ylabel("Sensitivity")
                plt.legend(["Classic Triggers", "Modified Triggers", "Signal Triggers"])
                plt.show()  
                
                # Plot Critical Success Indexes
                plt.plot(snrIterator, csiClassic, snrIterator, csiModified, snrIterator, csiSignal, linewidth = 0.8)
                plt.plot(snrIterator, [1]*len(snrIterator), 'r--', linewidth = 0.75)
                plt.title("SNR sweeping with fixed trigger at " + str(CORRELATION_THRESHOLD_LINE))
                plt.xlabel("Signal-to-Noise Ratio")
                plt.ylabel("Critical Success Index")
                plt.legend(["Classic Triggers", "Modified Triggers", "Signal Triggers"])
                plt.show()  

                # Plot Precision-Recall ratio. Remember: recall = sensitivity
                plt.plot(sensitivitiesClassic, precisionsClassic, sensitivitiesModified, precisionsModified, sensitivitiesSignal, precisionsSignal, linewidth = 0.8)
                plt.title("Precision-Recall plot for SNR sweeping at fixed threshold " + str(CORRELATION_THRESHOLD_LINE))
                plt.xlabel("Recall")
                plt.ylabel("Precision")
                plt.legend(["Classic Triggers", "Modified Triggers", "Signal Triggers"])
                plt.show()
                
                
            return{self.KEYWORDS_CLASSIC: triggersClassic,
                    self.KEYWORDS_MODIFIED : triggersModified,
                    self.KEYWORDS_SIGNAL: triggersSignal,
                    self.KEYWORDS_SENSITIVITY_CLASSIC : sensitivitiesClassic,
                    self.KEYWORDS_SENSITIVITY_MODIFIED : sensitivitiesModified,
                    self.KEYWORDS_SENSITIVITY_SIGNAL : sensitivitiesSignal,
                    self.KEYWORDS_PRECISION_CLASSIC : precisionsClassic,
                    self.KEYWORDS_PRECISION_MODIFIED : precisionsModified,
                    self.KEYWORDS_PRECISION_SIGNAL : precisionsSignal,
                    self.KEYWORDS_RECALL_CLASSIC : sensitivitiesClassic,
                    self.KEYWORDS_RECALL_MODIFIED : sensitivitiesModified,
                    self.KEYWORDS_RECALL_SIGNAL : sensitivitiesSignal,
                    self.KEYWORDS_CSI_CLASSIC : csiClassic,
                    self.KEYWORDS_CSI_MODIFIED : csiModified,
                    self.KEYWORDS_CSI_SIGNAL : csiSignal

            }
            
            return {self.KEYWORDS_CLASSIC: triggersClassic,
            self.KEYWORDS_MODIFIED : triggersModified,
            self.KEYWORDS_SIGNAL: triggersSignal,
            self.KEYWORDS_SENSITIVITY_CLASSIC : sensitivitiesClassic,
            self.KEYWORDS_SENSITIVITY_MODIFIED : sensitivitiesModified,
            self.KEYWORDS_SENSITIVITY_SIGNAL : sensitivitiesSignal,
            self.KEYWORDS_CSI_CLASSIC : csiClassic,
            self.KEYWORDS_CSI_MODIFIED : csiModified,
            self.KEYWORDS_CSI_SIGNAL : csiSignal
            }
            return {self.KEYWORDS_CLASSIC: triggersClassic, self.KEYWORDS_MODIFIED : triggersModified, self.KEYWORDS_SIGNAL: triggersSignal}
        
        


        # Perform a combined tests, including SNR and threshold sweeping
        # Returns a matrix with results of mixed tests. Also (if asked to) plots a 3D projection of the simulation
        # Method has been adapted to be run in multicore fashion. A new instance of the class must be created
        # for each new parallel thread and then, this method should be called. Unused methods MUST be deleted after
        # processing is done in order to avoid excessive RAM usage.
        def testsMixedSweeping(self, c, pulseLength, minPulseSeparation,
                               maxPulseSeparation, pulseType ,plot = False,
                               runMulticore = False, mpQueue = [],
                               pileUpBeta = False, outputFile = 0,):
            
            minSnr, maxSnr = self.getSnrLimits()
            snrStep = self.getSnrStepSize()
            
            minThreshold, maxThreshold = self.getThresholdLimits()
            thresholdStep = self.getThresholdStepSize()
            
            snrIterator = np.arange(minSnr, maxSnr + snrStep, snrStep)
            snrIterator = np.around(snrIterator, 2)
            
            thrIterator = np.arange(minThreshold, maxThreshold + thresholdStep, thresholdStep)
            thrIterator = np.around(thrIterator, 3)
            
            
            triggersClassic = []
            triggersModified = []
            triggersSignal = []
            
            sensitivitiesClassic = []
            sensitivitiesModified = []
            sensitivitiesSignal = []

            precisionsClassic = []
            precisionsModified = []
            precisionSignal = []
            
            csiClassic = []
            csiModified = []
            csiSignal = []
       
            for thisTrigger in thrIterator:
                print("\nProcessing trigger @ " + str(thisTrigger))

                
                thisSnrSweep = self.testsSnrSweeping(
                                    c = c,
                                    threshold = thisTrigger,
                                    pulseLength = pulseLength,
                                    pulseType = pulseType,
                                    minPulseSeparation = minPulseSeparation,
                                    maxPulseSeparation = maxPulseSeparation,
                                    plot = False,
                                    pileUp = bool(pileUpBeta),
                                    pileUpBeta = pileUpBeta
                                )
                
                triggersClassic.append(thisSnrSweep[self.KEYWORDS_CLASSIC])
                triggersModified.append(thisSnrSweep[self.KEYWORDS_MODIFIED])
                triggersSignal.append(thisSnrSweep[self.KEYWORDS_SIGNAL])
                
                sensitivitiesClassic.append(thisSnrSweep[self.KEYWORDS_SENSITIVITY_CLASSIC])
                sensitivitiesModified.append(thisSnrSweep[self.KEYWORDS_SENSITIVITY_MODIFIED])
                sensitivitiesSignal.append(thisSnrSweep[self.KEYWORDS_SENSITIVITY_SIGNAL])

                precisionsClassic.append(thisSnrSweep[self.KEYWORDS_PRECISION_CLASSIC])
                precisionsModified.append(thisSnrSweep[self.KEYWORDS_PRECISION_MODIFIED])
                precisionSignal.append(thisSnrSweep[self.KEYWORDS_PRECISION_SIGNAL])
                
                csiClassic.append(thisSnrSweep[self.KEYWORDS_CSI_CLASSIC])
                csiModified.append(thisSnrSweep[self.KEYWORDS_CSI_MODIFIED])
                csiSignal.append(thisSnrSweep[self.KEYWORDS_CSI_SIGNAL])
                
            if plot:
                self.plotResults3D( triggersClassic = triggersClassic,
                                triggersModified = triggersModified,
                                triggersSignal = triggersSignal,
                                snrIterator = snrIterator,
                                thrIterator = thrIterator,
                                )
                
                self.plotResults3D(
                    triggersClassic = sensitivitiesClassic,
                    triggersModified = sensitivitiesModified,
                    triggersSignal = sensitivitiesSignal,
                    snrIterator = snrIterator,
                    thrIterator = thrIterator
                )
                
                self.plotResults3D(
                    triggersClassic = csiClassic,
                    triggersModified = csiModified,
                    triggersSignal = csiSignal,
                    snrIterator = snrIterator,
                    thrIterator = thrIterator
                )
                
                
            if outputFile:
                import pandas as pd
                
                df = self.dataToDataFrame(triggersClassic = triggersClassic,
                                     triggersModified = triggersModified,
                                     triggersSignal = triggersSignal,
                                     snrIterator = snrIterator,
                                     thrIterator = thrIterator,
                                     
                                     sensitivityClassic = sensitivitiesClassic,
                                     sensitivityModified = sensitivitiesModified,
                                     sensitivitySignal = sensitivitiesSignal,

                                     precisionClassic = precisionsClassic,
                                     precisionModified = precisionsModified,
                                     precisionSignal = precisionSignal,

                                     recallClassic = sensitivitiesClassic,
                                     recallModified = sensitivitiesModified,
                                     recallSignal = sensitivitiesSignal,
                                     
                                     csiClassic = csiClassic,
                                     csiModified = csiModified,
                                     csiSignal = csiSignal
                                     )
                df.to_csv(outputFile)
           
                
            resultsDict = {
                        self.KEYWORDS_CLASSIC: triggersClassic,
                        self.KEYWORDS_MODIFIED : triggersModified,
                        self.KEYWORDS_SIGNAL: triggersSignal,
                        self.KEYWORDS_SNR_ITERATOR : snrIterator,
                        self.KEYWORDS_THRESHOLD_ITERATOR : thrIterator,
                        self.KEYWORDS_SENSITIVITY_CLASSIC : sensitivitiesClassic,
                        self.KEYWORDS_SENSITIVITY_MODIFIED : sensitivitiesModified,
                        self.KEYWORDS_SENSITIVITY_SIGNAL : sensitivitiesSignal,
                        self.KEYWORDS_PRECISION_CLASSIC : precisionsClassic,
                        self.KEYWORDS_PRECISION_MODIFIED : precisionsModified,
                        self.KEYWORDS_PRECISION_SIGNAL : precisionSignal,
                        self.KEYWORDS_RECALL_CLASSIC : sensitivitiesClassic,
                        self.KEYWORDS_RECALL_MODIFIED : sensitivitiesModified,
                        self.KEYWORDS_RECALL_SIGNAL : sensitivitiesSignal,
                        self.KEYWORDS_CSI_CLASSIC : csiClassic,
                        self.KEYWORDS_CSI_MODIFIED : csiModified,
                        self.KEYWORDS_CSI_SIGNAL : csiSignal
                    }

            if runMulticore:
                mpQueue.put(resultsDict)
            else:
                return resultsDict
               
        
        # Plot 3D representation of results
        def plotResults3D(self, snrIterator, thrIterator, triggersClassic, triggersModified, triggersSignal):
            xAxis = snrIterator
            yAxis = thrIterator
            
            X, Y = np.meshgrid(xAxis, yAxis)
            
            triggersClassic = np.array(triggersClassic)
            triggersModified = np.array(triggersModified)
            triggersSignal = np.array(triggersSignal)
            
            plt.figure()
            ax = plt.axes(projection = '3d')

            ax.plot_surface(X, Y, triggersClassic, cmap = 'Blues', alpha = 0.75, antialiased = True, label = 'Classic')
            ax.plot_surface(X, Y, triggersModified, cmap = 'Reds', alpha = 0.75, antialiased = True, label = 'Modified')
            # ax.plot_surface(X, Y, self.getNPulses() + 0*X + 0*Y, color = 'green', alpha = 0.25, antialiased = False, label = 'Expected triggers')
            ax.set_xlabel("Signal-to-noise ratio")
            ax.set_ylabel("Threshold")
            plt.show()
        
               
        #Convert results to an exportable format compatible with CSV. Returns data as a dataframe 
        def dataToDataFrame(self, triggersClassic,
                            triggersModified,
                            triggersSignal,
                            snrIterator,
                            thrIterator,
                            
                            sensitivityClassic = None,
                            sensitivityModified = None,
                            sensitivitySignal = None,

                            precisionClassic = None,
                            precisionModified = None,
                            precisionSignal = None,

                            recallClassic = None,
                            recallModified = None,
                            recallSignal = None,
                            
                            csiClassic = None,
                            csiModified = None,
                            csiSignal = None
                            
                            ):
            import pandas as pd
                
            # Data being preprocessed to be written into Pandas DF (and then, CSV)
            snrColumn = []
            thresholdsColumn = []
            classicColumn = []
            modifiedColumn = []
            signalColumn = []
            expectedColumn = [self.getNPulses()]*(len(snrIterator)*len(thrIterator))
            sensitivityClassicColumn = []
            sensitivityModifiedColumn = []
            sensitivitySignalColumn = []

            precisionClassicColumn = []
            precisionModifiedColumn = []
            precisionSignalColumn = []

            recallClassicColumn = []
            recallModifiedColumn = []
            recallSignalColumn = []

            csiClassicColumn = []
            csiModifiedColumn = []
            csiSignalColumn = []
            
            for i in range(len(snrIterator)):
                snrColumn.extend([snrIterator[i]] * len(thrIterator))
                thresholdsColumn.extend(thrIterator)
                for j in range(len(thrIterator)):
                    classicColumn.append(triggersClassic[j][i])
                    modifiedColumn.append(triggersModified[j][i])
                    signalColumn.append(triggersSignal[j][i])
                    
                    
                    # Optional metrics
                    if sensitivityClassic:
                        sensitivityClassicColumn.append(sensitivityClassic[j][i])
                        sensitivityModifiedColumn.append(sensitivityModified[j][i])
                        sensitivitySignalColumn.append(sensitivitySignal[j][i])

                    if precisionClassic:
                        precisionClassicColumn.append(precisionClassic[j][i])
                        precisionModifiedColumn.append(precisionModified[j][i])
                        precisionSignalColumn.append(precisionSignal[j][i])

                    if recallClassic:
                        recallClassicColumn.append(recallClassic[j][i])
                        recallModifiedColumn.append(recallModified[j][i])
                        recallSignalColumn.append(recallSignal[j][i])
            
                    if csiClassic:
                        csiClassicColumn.append(csiClassic[j][i])
                        csiModifiedColumn.append(csiModified[j][i])
                        csiSignalColumn.append(csiSignal[j][i])
            
            dfDict = {
                "SNR" : snrColumn,
                "Threshold" : thresholdsColumn,
                self.KEYWORDS_CLASSIC : classicColumn,
                self.KEYWORDS_MODIFIED : modifiedColumn,
                self.KEYWORDS_SIGNAL: signalColumn,
                "Expected" : expectedColumn
            }
            
            if sensitivityClassic:
                dfDict["Sensitivity_Classic"] = sensitivityClassicColumn
                dfDict["Sensitivity_Modified"] = sensitivityModifiedColumn
                dfDict["Sensitivity_Signal"] = sensitivitySignalColumn
            
            if precisionClassic:
                dfDict["Precision_Classic"] = precisionClassicColumn
                dfDict["Precision_Modified"] = precisionModifiedColumn
                dfDict["Precision_Signal"] = precisionSignalColumn

            if recallClassic:
                dfDict["Recall_Classic"] = recallClassicColumn
                dfDict["Recall_Modified"] = recallModifiedColumn
                dfDict["Recall_Signal"] = recallSignalColumn
                
            if csiClassic:
                dfDict["CSI_Classic"] = csiClassicColumn
                dfDict["CSI_Modified"] = csiModifiedColumn
                dfDict["CSI_Signal"] = csiSignalColumn
            
            df = pd.DataFrame.from_dict(dfDict)
            
            return df



        
        
        # Finds trigger positions by sweeping along the provided signal, the trigger polarity and trigger level
        # Polarity may be set using the constants self.POLARITY_POSITIVE or self.POLARITY_NEGATIVE
        # using cross-level trigger method. Returns a vector with the positions of the events that were found.
        def findTriggersCrossLevel(self, signal, triggerLevel, polarity, twoSamplesTrigger = False, isThisACorrelatedTrace = False, correlatedPulseShift = 0):          
            assert polarity == self.POLARITY_POSITIVE or polarity == self.POLARITY_NEGATIVE, "Polarity must be declared correctly"
            
            dataLen = len(signal)
            
            if type(signal) != np.ndarray:
                signal = np.array(signal)
                        
            triggerPositions = []

            if twoSamplesTrigger:
                j = 3
            else:
                j = 1
            
            # Although the loop code is duplicated (with swapped conditions), execution times are reduced
            # by keeping the polarity check out of each loop iteration
            if polarity == self.POLARITY_POSITIVE:
                while(j < dataLen):
                    if twoSamplesTrigger:
                        if(signal[j] > triggerLevel and signal[j - 1] >= triggerLevel and signal[j - 2] <= triggerLevel and signal[j - 3] < triggerLevel):
                            # If a trigger is found, add the precedent position to the list
                            triggerPositions.append(j - 2)
                    else:
                        if(signal[j] >= triggerLevel and signal[j - 1] < triggerLevel):
                            # If a trigger is found, add the precedent position to the list
                            triggerPositions.append(j - 1)
                    j += 1
            
            else:
                while(j < dataLen):
                    if twoSamplesTrigger:
                        if(signal[j] < triggerLevel and signal[j - 1] <= triggerLevel and signal[j - 2] >= triggerLevel and signal[j - 3] > triggerLevel):
                            # If a trigger is found, add the precedent position to the list
                            triggerPositions.append(j - 2)
                        
                    else:
                        if(signal[j] < triggerLevel and signal[j - 1] >= triggerLevel):
                            # If a trigger is found, add the precedent position to the list
                            triggerPositions.append(j - 1)
                    j += 1
                        

            triggerPositions = np.array(triggerPositions)

            # Correlated signals are located "N" positions in advance, where N is the coefficients (c) length
            if isThisACorrelatedTrace:
                triggerPositions += correlatedPulseShift

            return triggerPositions
            
                        
            

        # Computes the number of triggers within a list of triggers. A triggerPositions list/array must be provided.
        # Returns an integer with the trigger count in the list.
        def countTriggers(self, triggerPositions):
            return len(triggerPositions)
            
        
        # ================================================================
        # Looking for false negatives in the original pulse pattern trace
        # ================================================================
        def computeFalseNegatives(self, expectedPulsePositions, thisSignalTriggers, locationTolerance):
            
            fn = 0
            
            for i in expectedPulsePositions:
                j = i
                notFound = True
                
                # Looking within a specified tolerance range for each pulse trigger, based on original signal
                while(j < i + locationTolerance):
                    
                    # If the expected pulse is detected in the signal under test, no false negative
                    if(j in thisSignalTriggers):
                        notFound = False
                        j = i + locationTolerance
                    j += 1
                
                # However, if the pulse wasn't found in the expected location, a false negative count is added
                if notFound:
                    fn += 1

            return fn
        
        
        # =====================================================
        # Looking for true positives in the "experimental" data
        # =====================================================
        def computeTruePositives(self, expectedPulsePositions, thisSignalTriggers, locationTolerance, isThisACorrelatedTrace = True):
            
            tp = 0
            
            if isThisACorrelatedTrace:

                for i in thisSignalTriggers:
                    j = i - locationTolerance
                    
                    # Looking within a specified tolerance range for each pulse trigger, based on provided (new) signal
                    while(j <= i):
                        
                        # If the current experimental trigger is found within an expected position, a true positive count is added
                        if(j in expectedPulsePositions):
                            tp += 1
                            j = i + 1
                            
                        j += 1
            else:
                for i in expectedPulsePositions:
                    j = i + locationTolerance
                    
                    # Looking within a specified tolerance range for each pulse trigger, based on provided (new) signal
                    while(j >= i):
                        
                        # If the current experimental trigger is found within an expected position, a true positive count is added
                        if(j in thisSignalTriggers):
                            tp += 1
                            j = i - 1
                            
                        j -= 1
                    
            return tp
        
        
        
        # ======================================================
        # Looking for false positives in the "experimental" data
        # ======================================================
        def computeFalsePositives(self, expectedPulsePositions, thisSignalTriggers, locationTolerance, isThisACorrelatedTrace = True):
            
            fp = 0
            
            print("Total signal triggers: " + str(len(thisSignalTriggers)))

            if isThisACorrelatedTrace:
                for i in thisSignalTriggers:
                    j = i - locationTolerance
                    notFound = True
                    
                    while(j <= i):
                        
                        if(j in expectedPulsePositions):
                            notFound = False
                            j = i + 1
                        
                        j += 1
                        
                        if notFound:
                            fp += 1
            else:
                pass
                        
            return fp

        # ======================================================
        # Estimaging false positives in the "experimental" data
        # ======================================================
        def computeFalsePositivesV2(self, truePositivesCount, thisSignalTriggersCount):
            
            fp = thisSignalTriggersCount - truePositivesCount
            
            if fp < 0:
                fp = 0
                        
            return fp

        # ======================================================
        # Looking for True Negative events in experimental data
        # ======================================================

        # Since the True Negatives correspond to the succesfully "untriggered" signal that didn't contain any pulse,
        # the True Negatives are computed by subtracting the number of events (either false or positive) from the total
        # number of samples in the analyzed trace. Also, another 1 (unit) is subtracted to account for the two-samples trigger
        def computeTrueNegatives(self, traceSampleLen, tp, fp, fn):
            return traceSampleLen - tp - fp - fn - 1
        
        
        
        # Measures the sensitivity of the simulated trigger by counting true positive and false negative triggers
        # Returns sensitivity (real between 0 and 1)
        def computeSensitivity(self, expectedPulsePositions,
                               singlePulseLength, signalUnderTest,
                               triggerLevel, polarity,
                               indexSeekTolerance = 0.5,
                               isThisACorrelatedPulse = False):
            tp = 0  #True positives (expected triggers correctly found)
            fn = 0  #False negatives (expected triggers that were not found)
            fp = 0  #False positives (triggers that shouldn't exist)
            
            signalTriggers = self.findTriggersCrossLevel(
                signal = signalUnderTest,
                triggerLevel = triggerLevel,
                polarity = polarity,
                isThisACorrelatedTrace = isThisACorrelatedPulse,
                correlatedPulseShift = int(singlePulseLength * indexSeekTolerance),
                twoSamplesTrigger = False
                )
            
            print("Sensitivity. Trigger Level: " + str(triggerLevel))
            # print("Expected Pulse Positions: " + str(expectedPulsePositions))
            # print("This signal trigger positions: " + str(signalTriggers))
            
            locationTolerance = int(indexSeekTolerance * singlePulseLength)
            
            if isThisACorrelatedPulse:
                locationTolerance *= int(1 + triggerLevel*0.25)
            else:
                locationTolerance = int(singlePulseLength / 8)
                
            print("Location tolerance: " + str(locationTolerance))
            
            # ================================================================
            # Looking for false negatives in the original pulse pattern trace
            # ================================================================
            fn = self.computeFalseNegatives(expectedPulsePositions = expectedPulsePositions,
                                            thisSignalTriggers = signalTriggers,
                                            locationTolerance = locationTolerance)
            
            
            # =====================================================
            # Looking for true positives in the "experimental" data
            # =====================================================
            tp = self.computeTruePositives(expectedPulsePositions = expectedPulsePositions,
                                           thisSignalTriggers = signalTriggers,
                                           locationTolerance = locationTolerance,
                                           isThisACorrelatedTrace = isThisACorrelatedPulse)
         
             


            print("TP: " + str(tp))
            print("FN: " + str(fn))
            print("\n")
                        
            sensitivity = round(tp / (tp + fn), 8)
            
            
            # True Negatives (TN) can't be determined, as it's only the absence of pulses
            
            # Specificity (selectivity) = 1 - Sensitivity
            # Precision (positive predictive value) = TP / (TP + FP)
            # False discovery rate (FDR) = FP / (FP + TP)
            # Threat Score or Critical Success Index (CSI) = TP / (TP + FN + FP)
                        
            # Return the sensitivity of this measurement
            return sensitivity

        
        # Compute sensitivity based on pre-computed count parameters
        def computeSensitivityFromPreComputedParameters(self, tp, fn):
            return round(tp / (tp + fn), 8)



        # Recall is nothing else than the sensitivity (but with another name)
        # Just compute sensitivity if recall is required
        def computeRecall(self, expectedPulsePositions,
                               singlePulseLength, signalUnderTest,
                               triggerLevel, polarity,
                               indexSeekTolerance = 0.5,
                               isThisACorrelatedPulse = False):

            return self.computeSensitivity(expectedPulsePositions=expectedPulsePositions,
                                            singlePulseLength=singlePulseLength,
                                            signalUnderTest=signalUnderTest,
                                            triggerLevel=triggerLevel,
                                            polarity=polarity,
                                            indexSeekTolerance=indexSeekTolerance,
                                            isThisACorrelatedPulse=isThisACorrelatedPulse)
        
        # Compute recall based on pre-computed count parameters
        def computeRecallFromPreComputedParameters(self, tp, fn):
            return self.computeSensitivityFromPreComputedParameters(tp, fn)


        # Estimates the Precision of the simulated threshold by counting the true positive and false positive triggers.
        # Returns Precision (real between 0 and 1). Precision = TP / (TP + FP)
        def computePrecision(self, expectedPulsePositions,
                                singlePulseLength, signalUnderTest,
                                triggerLevel, polarity,
                                indexSeekTolerance = 0.5,
                                isThisACorrelatedPulse = False):
            tp = 0  #True positives (expected triggers correctly found)
            fp = 0  #False positives (triggers that shouldn't exist)

            signalTriggers = self.findTriggersCrossLevel(
                signal = signalUnderTest,
                triggerLevel = triggerLevel,
                polarity = polarity,
                isThisACorrelatedTrace = isThisACorrelatedPulse,
                correlatedPulseShift = int(singlePulseLength * indexSeekTolerance),
                twoSamplesTrigger = False
                )

            print("Precision. Trigger Level: " + str(triggerLevel))
            # print("Expected Pulse Positions: " + str(expectedPulsePositions))
            # print("This signal trigger positions: " + str(signalTriggers))

            locationTolerance = int(indexSeekTolerance * singlePulseLength)

            if isThisACorrelatedPulse:
                locationTolerance *= int(1 + triggerLevel*0.25)
            else:
                locationTolerance = int(singlePulseLength / 8)
                
            print("Location tolerance: " + str(locationTolerance))

            # =====================================================
            # Looking for true positives in the "experimental" data
            # =====================================================
            tp = self.computeTruePositives(expectedPulsePositions = expectedPulsePositions,
                                           thisSignalTriggers = signalTriggers,
                                           locationTolerance = locationTolerance,
                                           isThisACorrelatedTrace = isThisACorrelatedPulse)
         
             
            # ======================================================
            # Looking for false positives in the "experimental" data
            # ======================================================
            
            # fp = self.computeFalsePositives(expectedPulsePositions = expectedPulsePositions,
            #                                 thisSignalTriggers = signalTriggers,
            #                                 locationTolerance = locationTolerance)

            fp = self.computeFalsePositivesV2(truePositivesCount = tp,
                                                thisSignalTriggersCount = len(signalTriggers))

            print("TP: " + str(tp))
            print("FP: " + str(fp))
            print("\n")
            
            div = tp + fp
            if div != 0:           
                precision = round(tp / div, 8)
            else:
                precision = np.nan
            
            return precision


         # Compute precision based on pre-computed count parameters
        def computePrecisionFromPreComputedParameters(self, tp, fp):
            div = tp + fp
            if div != 0:           
                precision = round(tp / div, 8)
            else:
                precision = np.nan
            
            return precision

        # Measures the critical success index (CSI) of the simulated trigger by counting true positive and false negative triggers
        # Returns CSI (real between 0 and 1). CSI = TP / (TP + FN + FP)
        def computeCriticalSuccessIndex(self, expectedPulsePositions,
                               singlePulseLength, signalUnderTest,
                               triggerLevel, polarity,
                               indexSeekTolerance = 0.5,
                               isThisACorrelatedPulse = False,
                               returnRawCounts = False):
            
            signalTriggers = self.findTriggersCrossLevel(
                signal = signalUnderTest,
                triggerLevel = triggerLevel,
                polarity = polarity,
                isThisACorrelatedTrace = isThisACorrelatedPulse,
                correlatedPulseShift = int(singlePulseLength * indexSeekTolerance),
                twoSamplesTrigger = False
                )
            
            print("CSI. Trigger Level: " + str(triggerLevel))
            # print("Expected Pulse Positions: " + str(expectedPulsePositions))
            # print("This signal trigger positions: " + str(signalTriggers))
            
            locationTolerance = int(indexSeekTolerance * singlePulseLength)
            
            if isThisACorrelatedPulse:
                locationTolerance *= int(1 + triggerLevel*0.25)
            else:
                locationTolerance = int(singlePulseLength / 8)
                
            print("Location tolerance: " + str(locationTolerance))
            
            # ================================================================
            # Looking for false negatives in the original pulse pattern trace
            # ================================================================
            fn = self.computeFalseNegatives(expectedPulsePositions = expectedPulsePositions,
                                            thisSignalTriggers = signalTriggers,
                                            locationTolerance = locationTolerance)
            
            
            # =====================================================
            # Looking for true positives in the "experimental" data
            # =====================================================
            tp = self.computeTruePositives(expectedPulsePositions = expectedPulsePositions,
                                           thisSignalTriggers = signalTriggers,
                                           locationTolerance = locationTolerance,
                                           isThisACorrelatedTrace = isThisACorrelatedPulse)
         
             
            # ======================================================
            # Looking for false positives in the "experimental" data
            # ======================================================
            
            # fp = self.computeFalsePositives(expectedPulsePositions = expectedPulsePositions,
            #                                 thisSignalTriggers = signalTriggers,
            #                                 locationTolerance = locationTolerance)

            fp = self.computeFalsePositivesV2(truePositivesCount = tp,
                                                thisSignalTriggersCount = len(signalTriggers))


            print("TP: " + str(tp))
            print("FN: " + str(fn))
            print("FP: " + str(fp))
            print("\n")
                        
            
            csi = round(tp / (tp + fn + fp), 8)
            
            if returnRawCounts:
                return csi, (tp, fn, fp)
            else:           
                return csi
                        

        # Compute CSI based on pre-computed count parameters                
        def computeCriticalSuccessIndexFromPreComputedParameters(self, tp, fn, fp):
            csi = round(tp / (tp + fn + fp), 8)
            
            return csi

        # True Positive Rate (TPR) is nothing else than the sensitivity (but with another name)
        # Just compute sensitivity if TPR is required
        def computeTruePositiveRate(self, expectedPulsePositions,
                               singlePulseLength, signalUnderTest,
                               triggerLevel, polarity,
                               indexSeekTolerance = 0.5,
                               isThisACorrelatedPulse = False):

            return self.computeSensitivity(expectedPulsePositions=expectedPulsePositions,
                                            singlePulseLength=singlePulseLength,
                                            signalUnderTest=signalUnderTest,
                                            triggerLevel=triggerLevel,
                                            polarity=polarity,
                                            indexSeekTolerance=indexSeekTolerance,
                                            isThisACorrelatedPulse=isThisACorrelatedPulse)


        # Compute True Positive Rate (TPR) from pre computed parameters
        def computeTruePositiveRateFromPreComputedParameters(self, tp, fn):
            return self.computeSensitivityFromPreComputedParameters(tp, fn)


        # Compute False Positive Rate (FPR)
        def computeFalsePositiveRate(self, expectedPulsePositions,
                                singlePulseLength, signalUnderTest,
                                triggerLevel, polarity,
                                indexSeekTolerance = 0.5,
                                isThisACorrelatedPulse = False):
        
            signalTriggers = self.findTriggersCrossLevel(
                signal = signalUnderTest,
                triggerLevel = triggerLevel,
                polarity = polarity,
                isThisACorrelatedTrace = isThisACorrelatedPulse,
                correlatedPulseShift = int(singlePulseLength * indexSeekTolerance),
                twoSamplesTrigger = False
                )

            print("FPR. Trigger Level: " + str(triggerLevel))

            locationTolerance = int(indexSeekTolerance * singlePulseLength)
            
            if isThisACorrelatedPulse:
                locationTolerance *= int(1 + triggerLevel*0.25)
            else:
                locationTolerance = int(singlePulseLength / 8)
                
            print("Location tolerance: " + str(locationTolerance))
            
            # ================================================================
            # Looking for false negatives in the original pulse pattern trace
            # ================================================================
            fn = self.computeFalseNegatives(expectedPulsePositions = expectedPulsePositions,
                                            thisSignalTriggers = signalTriggers,
                                            locationTolerance = locationTolerance)
            
            
            # =====================================================
            # Looking for true positives in the "experimental" data
            # =====================================================
            tp = self.computeTruePositives(expectedPulsePositions = expectedPulsePositions,
                                           thisSignalTriggers = signalTriggers,
                                           locationTolerance = locationTolerance,
                                           isThisACorrelatedTrace = isThisACorrelatedPulse)
         
             
            # ======================================================
            # Looking for false positives in the "experimental" data
            # ======================================================
            
            # fp = self.computeFalsePositives(expectedPulsePositions = expectedPulsePositions,
            #                                 thisSignalTriggers = signalTriggers,
            #                                 locationTolerance = locationTolerance)

            fp = self.computeFalsePositivesV2(truePositivesCount = tp,
                                                thisSignalTriggersCount = len(signalTriggers))


            # ======================================================
            # Estimating True Negatives based on TP, FN, FP counts
            # ======================================================
            tn = self.computeTrueNegatives(traceSampleLen = len(signalUnderTest),
                                        tp = tp,
                                        fp = fp,
                                        fn = fn)
            print("SignalLen: " + str(len(signalUnderTest)))
            print("TP: " + str(tp))
            print("FN: " + str(fn))
            print("FP: " + str(fp))
            print("TN: " + str(tn))
            print("\n")

            fpr = round(fp / (fp + tn), 8)

            return fpr

            


        # Compute False Positive Rate (FPR) from pre computed parameters
        def computeFalsePositiveRateFromPreComputedParameters(self, fp, tn):
            return round(fp / (fp + tn), 8)
            
        
        '''
        ===================
        Getters and setters
        ===================
        '''

        def setSignalAmp(self, value):
            self.signalAmp = value
                 
        def getThresholdLimits(self):
            return self.__thresholdLimits
        
        def getThresholdStepSize(self):
            return self.__thresholdStepSize
        
        def getSnrLimits(self):
            return self.__snrLimits
        
        def getSnrStepSize(self):
            return self.__snrStepSize
        
        def getNPulses(self):
            return self.__nPulses

        def getSignalAmp(self):
            return self.signalAmp


        

        


















if __name__ == '__main__':


    cc = CorrelationComparison()
    signal = PulseSynthesis()
    
    TEST_PULSE_LEN = 64  
    # TEST_PULSE_LEN = 25 # =========== REVERT THIS VALUE AFTER ECAL TEST ===========
    PULSE_TYPE = signal.pmtPulse
    SIGNAL_TO_NOISE_RATIO = 3
    THRESHOLD_SIGMAS = 3
    ORIGINAL_PULSE_REPLICAS = 10
    LEADING_TO_LENGTH_RATIO = 0.45
    DISTANCE_BETWEEN_PULSES = [TEST_PULSE_LEN * 0.25 , TEST_PULSE_LEN * 10]
    LOW_FREQ_NOISE_INDEX = 0.00
    
    #DO NOT MODIFY THESE CONSTANTS
    # NOISE_AMPLITUDE = signal.snrToSigma(SIGNAL_TO_NOISE_RATIO, MAX_PULSE_AMPLITUDE)
    
    
    # Ideal pulse used to generate coefficients
    c = PULSE_TYPE(
            length = TEST_PULSE_LEN,
            amplitude = 1,
            noiseSigma = 0, #Ideal pulse means NO NOISE
            leadingProportion = LEADING_TO_LENGTH_RATIO
        )
    
    
    '''
    =========== REMOVE THESE LINES AFTER ECAL TEST ===========
    
    c = signal.ecal2Pulse(length = TEST_PULSE_LEN, amplitude = 1, tau = 1.25, timeOfArrival = 5, reflectionAmplitude = 0.17094, reflectionDelay = 4.27578,
                          noiseSigma = 0, verticalOffset = 0, quantizationBits = 0)
    
    # c = signal.ecal2PulseFromSamples(length = TEST_PULSE_LEN, amplitude = 1, tau = 1.25, timeOfArrival = 0, reflectionAmplitude = 0.17094, reflectionDelay = 4.27578,
    #                       noiseSigma = 0, verticalOffset = 0, quantizationBits = 0)
    
    
    =========== REMOVE THESE LINES AFTER ECAL TEST ===========
    '''
    
    
    '''
    ======= REMOVE THESE LINES AFTER GAMMA-NEUTRON TEST ==========
    cGamma = signal.ej276GammaPulse(length = 500)
    cNeutron = signal.ej276NeutronPulse(length = 500)
    
    plt.plot(np.arange(0,500), cGamma, 'red', label = 'Gamma rays')
    plt.plot(np.arange(0,500), cNeutron, 'black', label = 'Fast neutrons')
    plt.legend(["Gamma rays", "Fast neutrons"])
    # plt.yscale('log')
    plt.show()
    ======== REMOVE THESE LINES AFTER GAMMA-NEUTRON TEST =========
    '''
    
    
    NOISE_AMPLITUDE = 1 # Setting noise sigma = 1. DO NOT MODIFY THIS VALUE
    MAX_PULSE_AMPLITUDE = signal.snrToSignalAmp(snr = SIGNAL_TO_NOISE_RATIO)
    MIN_PULSE_AMPLITUDE = MAX_PULSE_AMPLITUDE # Equal pulse amplitudes
    # MIN_PULSE_AMPLITUDE = MAX_PULSE_AMPLITUDE * 10 # =========== REVERT THIS VALUE AFTER ECAL TEST ===========
    


    #Visual line value to show "valid" correlation threshold
    CORRELATION_THRESHOLD_LINE = round((1 / SIGNAL_TO_NOISE_RATIO) * THRESHOLD_SIGMAS, 4)


    #Noisy synthetic signal
    x, xPos = signal.pmtTrace(
        nPulses = ORIGINAL_PULSE_REPLICAS,
        pulseLength = TEST_PULSE_LEN,
        pulseType = signal.pmtPulse,
        leadingProportion =  LEADING_TO_LENGTH_RATIO,
        minAmp=MIN_PULSE_AMPLITUDE,
        maxAmp=MAX_PULSE_AMPLITUDE,
        traceNoiseSigma=NOISE_AMPLITUDE,
        lowFreqNoiseContribution=LOW_FREQ_NOISE_INDEX,
        minSeparation = DISTANCE_BETWEEN_PULSES[0],
        maxSeparation = DISTANCE_BETWEEN_PULSES[1],
        returnPulseLocations = True,
        plotNoiseDist = True,
        # amplitudeFromSNR = SIGNAL_TO_NOISE_RATIO
    )
    
    
    
    
    
    '''
    =========== REMOVE THESE LINES AFTER ECAL TEST ===========
    
    # ECAL-based pulse model
    x, xPos = signal.ecalTrace(
        nPulses = ORIGINAL_PULSE_REPLICAS,
        pulseLength = TEST_PULSE_LEN,
        # pulseType = signal.ecal2PulseFromSamples,
        pulseType = signal.ecal2Pulse,
        minAmp=MIN_PULSE_AMPLITUDE,
        maxAmp=MAX_PULSE_AMPLITUDE,
        traceNoiseSigma=NOISE_AMPLITUDE,
        tau = 1.25, k = 0.17094, dt = 4.27578,
        minSeparation = DISTANCE_BETWEEN_PULSES[0],
        maxSeparation = DISTANCE_BETWEEN_PULSES[1],
        returnPulseLocations = True,
        plotNoiseDist = True,
        leadingZeroes = 500,
        trailingZeroes = 100
    )
    
    =========== REMOVE THESE LINES AFTER ECAL TEST ===========
    '''
    
    
    
    
    
    numPulses = len(xPos)
  
    


    '''
    ======================
    Self-correlation tests
    ======================
    '''
    t = np.arange(len(c))

    # Pearson classic
    print("Classic with itself: {:.4f}".format(cc.pearsonClassic(x, x)))
    # print("Classic with noisy signal: {:.4f}".format(cc.pearsonClassic(x, c)))
    
    # Pearson modified
    print("Modified with itself: {:.4f}".format(cc.pearsonModified(x, x)))
    

    #Plotting self-correlation results
    plt.plot(t, c, label = "Synthetic pulse", color = 'red', marker = ".", linewidth = 1)
    plt.title("Pattern signal ($c$)")
    plt.ylabel("Amplitude (a.u.)")
    plt.xlabel("Samples")
    plt.show()


    
    



    '''
    ================================
    Sliding window correlation tests
    ================================
    '''

    # cTests = CorrelationTests(thresholdLimits = [0.01, 1.5],
    #                           thresholdStepSize = 0.01,
    #                           snrLimits = [1, 8],
    #                           snrStepSize = 0.5
    #                           )
    
    # cTests = CorrelationTests(thresholdLimits = [0.2, 1.0],
    #                           thresholdStepSize = 0.1,
    #                           snrLimits = [1, 4],
    #                           snrStepSize = 1,
    #                           nPulses = ORIGINAL_PULSE_REPLICAS  
    #                           )

    cTests = CorrelationTests(thresholdLimits = [0.0, 1.75],
                              thresholdStepSize = 0.025,
                              snrLimits = [1, 10],
                              snrStepSize = 0.25,
                              nPulses = ORIGINAL_PULSE_REPLICAS  
                              )

    thisCorrelations = cTests.computeSingleCorrelations(x, c, plot = True, signalAmplitude = MAX_PULSE_AMPLITUDE,
                                                        correlationThresholdLine = CORRELATION_THRESHOLD_LINE, 
                                                        avoidSignalNormalization = False)
    
    import pandas as pd
    dfX = pd.DataFrame(x)
    dfC = pd.DataFrame(c)
    dfPc = pd.DataFrame(thisCorrelations[cTests.KEYWORDS_CLASSIC])
    dfPm = pd.DataFrame(thisCorrelations[cTests.KEYWORDS_MODIFIED])
    dfX.to_csv("./traces/signalSNR" + str(SIGNAL_TO_NOISE_RATIO) + ".csv", index = False, header = False)
    dfC.to_csv("./traces/pattern.csv", index = False, header = False)
    dfPm.to_csv("./traces/correlation_Classic_SNR" + str(SIGNAL_TO_NOISE_RATIO) + ".csv", index = False, header = False)
    dfPm.to_csv("./traces/correlation_Modified_SNR" + str(SIGNAL_TO_NOISE_RATIO) + ".csv", index = False, header = False)

    '''
    =======================
    Threshold sweeping test
    =======================
    '''
    
    thresholdSweepingResults = cTests.testThresholdSweeping(signal = thisCorrelations["Signal"],
                                 signalPulsePositions = xPos,
                                 pearsonClassic = thisCorrelations["Classic"],
                                 pearsonModified = thisCorrelations["Modified"],
                                 maxTraceAmplitude = MAX_PULSE_AMPLITUDE,
                                 plot = True,
                                 singlePulseLen = TEST_PULSE_LEN                   
    )

    print(thresholdSweepingResults)
    
    
    # '''
    # =================
    # SNR sweeping test
    # =================
    # '''
    
    # snrResults = cTests.testsSnrSweeping(
    #     c = c,
    #     threshold = CORRELATION_THRESHOLD_LINE,
    #     pulseLength = TEST_PULSE_LEN,
    #     pulseType = PULSE_TYPE,
    #     minPulseSeparation = DISTANCE_BETWEEN_PULSES[0],
    #     maxPulseSeparation = DISTANCE_BETWEEN_PULSES[1],
    #     plot = True,
    #     correlationThresholdLine = CORRELATION_THRESHOLD_LINE,
    # )
    
    # print(snrResults)
    
    
    # '''
    # ================================================
    # Mixed test: sweeping both, SNR and Trigger Level
    # ================================================
    # '''
    
    # startTime = time()

    # mixedResults = cTests.testsMixedSweeping(
    #     c = c,
    #     pulseLength = TEST_PULSE_LEN,
    #     pulseType = PULSE_TYPE,
    #     minPulseSeparation = DISTANCE_BETWEEN_PULSES[0],
    #     maxPulseSeparation = DISTANCE_BETWEEN_PULSES[1],
    #     plot = False,
    #     outputFile = './multiMetrics.csv'
    # )
    

    # endTime = time()


    # print("Seconds elapsed: " + str(int(endTime - startTime)))
