#!/bin/python3

'''
This script is used to directly generate plots
for the publications/papers and export them to PDF.
'''

from isort import file
from pearsonComparison import PulseSynthesis
import numericPearson
import numpy as np
import os
from matplotlib import rc
import matplotlib.pyplot as plt
from labellines import labelLines
import pandas as pd

pd.options.mode.chained_assignment = None  # default='warn'

DEFAULT_FONT_SIZE = 8
def setFontSize(fontSize = DEFAULT_FONT_SIZE):
    plt.rcParams["font.size"] = fontSize    
plt.rcParams["font.family"] = "Serif"


PEARSON_CLASSIC_COLOR = 'green'
PEARSON_MODIFIED_COLOR = 'darkorange'
SIGNAL_COLOR = 'tab:blue'
CORRELATION_DIFFERENCE_COLOR = 'tab:purple'

# Plotting global parameters
setFontSize()
currentDir = os.getcwd().rstrip('/')
# resultsFile = currentDir + '/scripts/simulationResults/pulseTypepmtPulse_pulseLen64_nPulses1000_thrS0.1_thrE1.75_snrS1_snrE10.csv'
resultsFile = currentDir + '/scripts/simulationResults/pulseTypepmtPulse_pulseLen64_nPulses1000_thrS0.1_thrE1.75_snrS1_snrE10_pileUp5.csv'
hwResultsFile = currentDir + '/scripts/simulationResults/hlsResults.csv'


# plt.rc('text', usetex=True)
# plt.rc('font', family='serif')
# plt.xlabel(r'Coefficient Index ',fontsize=12, labelpad=5)
# plt.ylabel(r'Amplitude (Arbitrary Units)',fontsize=12)
# ####plt.suptitle(r'DPLMS FIR Coefficients', fontsize=16)
# plt.plot(long_fir_opt)
# axes=plt.gca()
# axes.set_aspect(1.0/axes.get_data_ratio() * ratio)
# plt.savefig('plots/dplms_coef.pdf', bbox_inches = 'tight')
# plt.close()


'''
==========================================================================================
                                Base results class
==========================================================================================
'''
class Results(object):
    def __init__(self, resultsFile, fontSize, currentDir = './'):
        # Constants declaration
        self.KEYWORDS_THRESHOLD = "Threshold"
        self.KEYWORDS_SNR = "SNR"
        self.KEYWORDS_TRIGGERCOUNT_CLASSIC = "Classic"
        self.KEYWORDS_TRIGGERCOUNT_MODIFIED = "Modified"
        self.KEYWORDS_TRIGGERCOUNT_SIGNAL = "Signal"
        self.KEYWORDS_TRIGGERCOUNT_EXPECTED = "Expected"
        self.KEYWORDS_PLOT_CSI_CLASSIC = "CSI_Classic"
        self.KEYWORDS_PLOT_CSI_MODIFIED = "CSI_Modified"
        self.KEYWORDS_PLOT_CSI_SIGNAL = "CSI_Signal"
        self.KEYWORDS_PLOT_PRECISION_CLASSIC = "Precision_Classic"
        self.KEYWORDS_PLOT_RECALL_CLASSIC = "Recall_Classic"
        self.KEYWORDS_PLOT_PRECISION_MODIFIED = "Precision_Modified"
        self.KEYWORDS_PLOT_RECALL_MODIFIED = "Recall_Modified"
        self.KEYWORDS_PLOT_PRECISION_SIGNAL = "Precision_Signal"
        self.KEYWORDS_PLOT_RECALL_SIGNAL = "Recall_Signal"
        self.KEYWORDS_AUC_PR_CLASSIC = "AUC_PR_Classic"
        self.KEYWORDS_AUC_PR_MODIFIED = "AUC_PR_Modified"
        self.KEYWORDS_AUC_PR_SIGNAL = "AUC_PR_Signal"
        self.KEYWORDS_TRACE_CLASSIC_SIM = "PCC_Simulation"
        self.KEYWORDS_TRACE_CLASSIC_HW  = "PCC_HLS"
        self.KEYWORDS_TRACE_MODIFIED_SIM = "SPCC_Simulation"
        self.KEYWORDS_TRACE_MODIFIED_HW = "SPCC_HLS"
        self.KEYWORDS_TRACE_CLASSIC_DIFF = "PCC_Diff"
        self.KEYWORDS_TRACE_MODIFIED_DIFF = "SPCC_Diff"
        
        if fontSize != False:
            self.fontSize = fontSize
        
        if resultsFile != False:
            self.df = pd.read_csv(resultsFile, index_col = 0)
            self.currentDir = currentDir
    
    def getDataset(self):
        return self.df

'''
==========================================================================================
                        Results-related inherited plot class
==========================================================================================
'''

class PlotResults(Results):
    def __init__(self, resultsFile, fontSize, currentDir = './'):
        super().__init__(resultsFile, fontSize, currentDir)
        self.auxComputation = ResultsComputations()
         
    # Plot the results of the threshold sweep for a fixed SNR value 
    def plotThresholdSweepFixedSNR_CSI(self, snrValue, plotOriginal = True,
                                       plotModified = True, plotSignal = False,
                                       thresholdLimits = None, pltRatio = 0.8,
                                       outputFileName = 'thresholdSweepFixedSNR_CSI.pdf',
                                       interpolate = False):
        if not thresholdLimits:
            df = self.df.loc[self.df[self.KEYWORDS_SNR] == snrValue]
        else:
            df = self.df.loc[((self.df[self.KEYWORDS_SNR] == snrValue) & (self.df[self.KEYWORDS_THRESHOLD] >= thresholdLimits[0]) & (self.df[self.KEYWORDS_THRESHOLD] <= thresholdLimits[1]))]
            plt.xlim(thresholdLimits)
        
        newDf = pd.DataFrame()
        
        newDf = df[[self.KEYWORDS_SNR, self.KEYWORDS_THRESHOLD]]
        
        setFontSize(self.fontSize - 0)
        
        plt.title("Critical Success Index vs. Threshold value \n PSNR = " + str(snrValue))
        plt.xlabel("Trigger threshold")
        plt.ylabel("Critical Success Index")
        
        pltLegend = []
        
          
        
        if plotOriginal:
            x = newDf[self.KEYWORDS_THRESHOLD]  
            y = df[self.KEYWORDS_PLOT_CSI_CLASSIC]
            if interpolate:
                (x, y) = self.auxComputation.interpolateCurve(x, y, interpolate)
            
            plt.plot(x, y, linewidth = 1.0, color = PEARSON_CLASSIC_COLOR)
            pltLegend.append("PCI")
        
        if plotModified:
            x = newDf[self.KEYWORDS_THRESHOLD]  
            y = df[self.KEYWORDS_PLOT_CSI_MODIFIED]
            if interpolate:
                (x, y) = self.auxComputation.interpolateCurve(x, y, interpolate)
                
            plt.plot(x, y, linewidth = 1.0, color = PEARSON_MODIFIED_COLOR)
            pltLegend.append("SPCI")
            
        if plotSignal:
            x = newDf[self.KEYWORDS_THRESHOLD]  
            y = df[self.KEYWORDS_PLOT_CSI_SIGNAL]
            if interpolate:
                (x, y) = self.auxComputation.interpolateCurve(x, y, interpolate)
                
            plt.plot(x, y, label = "CSI Signal")
            pltLegend.append("Signal")
        
        
        plt.legend(pltLegend, loc = 'upper left', fontsize = DEFAULT_FONT_SIZE - 1)
        plt.ylim([0, 1.01])
        
        plt.axhline(y = 1.0, color = 'gray', linewidth = 0.5, linestyle = '--')
        
        axes=plt.gca()
        axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)

        plt.savefig(currentDir + '/scripts/plots/results_' + outputFileName, bbox_inches = 'tight')
        plt.close()
        
        setFontSize(self.fontSize)
    
    
    
    
    
    # Plot the results of the SNR sweep for a fixed threshold value
    def plotSNRSweepFixedThreshold_CSI(self, thresholdValue, plotOriginal = True,
                                       plotModified = True, plotSignal = False,
                                       snrLimits = None, pltRatio = 0.8,
                                       outputFileName = 'snrSweepFixedThreshold_CSI.pdf',
                                       interpolate = False):
        if not snrLimits:
            df = self.df.loc[self.df[self.KEYWORDS_THRESHOLD] == thresholdValue]
        else:
            df = self.df.loc[((self.df[self.KEYWORDS_THRESHOLD] == thresholdValue) & (self.df[self.KEYWORDS_SNR] >= snrLimits[0]) & (self.df[self.KEYWORDS_SNR] <= snrLimits[1]))]
            plt.xlim(snrLimits)
        
        newDf = pd.DataFrame()
        
        newDf = df[[self.KEYWORDS_SNR, self.KEYWORDS_THRESHOLD]]
        
        setFontSize(self.fontSize - 0)
        
        # plt.title("Critical Success Index vs. PSNR value \n Threshold = " + str(thresholdValue))
        plt.title("Threshold = " + str(thresholdValue))
        plt.xlabel("PSNR")
        plt.ylabel("Critical Success Index")
        
        pltLegend = []
        
        if plotOriginal:
            x = newDf[self.KEYWORDS_SNR]
            y = df[self.KEYWORDS_PLOT_CSI_CLASSIC]
            
            if interpolate:
                (x,y) = self.auxComputation.interpolateCurve(x, y, int(interpolate))
                
            plt.plot(x, y, linewidth = 1.0, color = PEARSON_CLASSIC_COLOR)
            pltLegend.append("PCI")
        
        if plotModified:
            x = newDf[self.KEYWORDS_SNR]
            y = df[self.KEYWORDS_PLOT_CSI_MODIFIED]
            
            if interpolate:
                (x,y) = self.auxComputation.interpolateCurve(x, y, int(interpolate))
            
            plt.plot(x, y, linewidth = 1.0, color = PEARSON_MODIFIED_COLOR)
            pltLegend.append("SPCI")
            
        if plotSignal:
            x = newDf[self.KEYWORDS_SNR]
            y = df[self.KEYWORDS_PLOT_CSI_SIGNAL]
            
            if interpolate:
                (x,y) = self.auxComputation.interpolateCurve(x, y, int(interpolate))
            
            plt.plot(x, y, label = "CSI Signal")
            pltLegend.append("Signal")
        
        
        plt.legend(pltLegend, loc = 'lower right', fontsize = DEFAULT_FONT_SIZE - 1)
        plt.ylim([0, 1.01])
        
        plt.axhline(y = 1.0, color = 'gray', linewidth = 0.5, linestyle = '--')
        
        axes=plt.gca()
        axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)

        plt.savefig(currentDir + '/scripts/plots/results_' + outputFileName, bbox_inches = 'tight')
        plt.close()
        
        setFontSize(self.fontSize)
            
        
        
        
    # Plot the results of the threshold sweep for a family of SNR values/curves
    def plotThresholdSweepSNRFamilyCurves_CSI(self, snrValues = [], plotOriginal = True,
                                       plotModified = True, plotSignal = False,
                                       thresholdLimits = None, pltRatio = 0.8,
                                       outputFileName = 'thresholdSweepSNRFamilyCurves_CSI.pdf',
                                       xLabelPositionLimits = (0.45, 1.1), verticalTriggerLine = False):
        if not thresholdLimits:
            df = self.df.loc[self.df[self.KEYWORDS_SNR].isin(snrValues)]
        else:
            df = self.df.loc[((self.df[self.KEYWORDS_SNR].isin(snrValues)) & (self.df[self.KEYWORDS_THRESHOLD] >= thresholdLimits[0]) & (self.df[self.KEYWORDS_THRESHOLD] <= thresholdLimits[1]))]
            plt.xlim(thresholdLimits)
        
        newDf = pd.DataFrame()
        
        newDf = df[[self.KEYWORDS_SNR, self.KEYWORDS_THRESHOLD]]
        newDf[self.KEYWORDS_PLOT_CSI_CLASSIC] = df[self.KEYWORDS_PLOT_CSI_CLASSIC]
        newDf[self.KEYWORDS_PLOT_CSI_MODIFIED] = df[self.KEYWORDS_PLOT_CSI_MODIFIED]
        newDf[self.KEYWORDS_PLOT_CSI_SIGNAL] = df[self.KEYWORDS_PLOT_CSI_SIGNAL]
        
        setFontSize(self.fontSize - 0)
        
        # plt.title("Critical success index vs. Threshold value \n PSNR values = " + str(snrValues))
        plt.title("PSNR = " + str(snrValues))
        plt.xlabel("Threshold level")
        plt.ylabel("Critical success index")
        plt.rcParams["figure.autolayout"] = True
        
        pltLegend = []
        pltValues = []
        
        for thisSnr in snrValues:
            
            thisDf = newDf.loc[newDf[self.KEYWORDS_SNR] == thisSnr]
            
            if len(thisDf) > 0:
        
                if plotOriginal:                
                    
                    plt.plot(thisDf[self.KEYWORDS_THRESHOLD], thisDf[self.KEYWORDS_PLOT_CSI_CLASSIC], linewidth = 0.75, color = PEARSON_CLASSIC_COLOR, label = str(thisSnr))
                    # pltLegend.append("PCI @ SNR = " + str(thisSnr))
                
                if plotModified:
                    
                    plt.plot(thisDf[self.KEYWORDS_THRESHOLD], thisDf[self.KEYWORDS_PLOT_CSI_MODIFIED], linewidth = 0.75, color = PEARSON_MODIFIED_COLOR, label = str(thisSnr))
                    # pltLegend.append("SPCI @ SNR = " + str(thisSnr))
                    
                if plotSignal:
                    
                    plt.plot(thisDf[self.KEYWORDS_THRESHOLD], thisDf[self.KEYWORDS_PLOT_CSI_SIGNAL], linewidth = 0.5, label = str(thisSnr))
                    # pltLegend.append("Signal @ SNR = " + str(thisSnr))
            
            else:
                print("Warning: no data for SNR = " + str(thisSnr))

        
        pltLegend = ["PCI", "SPCI"]
        
        labelLines(plt.gca().get_lines(), zorder=2.0, align = False, shrink_factor = 0.05, xvals = xLabelPositionLimits)
        
        plt.legend(pltLegend, loc = 'upper left', fontsize = DEFAULT_FONT_SIZE - 1)
        plt.ylim([0, 1.01])
        
        plt.axhline(y = 1.0, color = 'gray', linewidth = 0.5, linestyle = '--')
        
        if verticalTriggerLine:
            plt.axvline(x = verticalTriggerLine, color = 'maroon', linewidth = 0.5, linestyle = '--')
        
        axes=plt.gca()
        axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)
        axes.grid(which='major', axis='both', linestyle='--', linewidth = 0.35)

        plt.savefig(currentDir + '/scripts/plots/results_' + outputFileName, bbox_inches = 'tight')
        plt.close()
        
        setFontSize(self.fontSize)






    # Plot the results of the threshold sweep for a fixed SNR value 
    def plotThresholdSweepFixedSNR_PR(self, snrValue, plotOriginal = True,
                                       plotModified = True, plotSignal = True,
                                       thresholdLimits = None, pltRatio = 1.0,
                                       outputFileName = 'thresholdSweepFixedSNR_PR.pdf',
                                       showTitle = True):
        if not thresholdLimits:
            df = self.df.loc[self.df[self.KEYWORDS_SNR] == snrValue]
        else:
            df = self.df.loc[((self.df[self.KEYWORDS_SNR] == snrValue) & (self.df[self.KEYWORDS_THRESHOLD] >= thresholdLimits[0]) & (self.df[self.KEYWORDS_THRESHOLD] <= thresholdLimits[1]))]
            plt.xlim(thresholdLimits)
        
        newDf = pd.DataFrame()
        
        newDf = df[[self.KEYWORDS_SNR, self.KEYWORDS_THRESHOLD]]
        
        setFontSize(self.fontSize - 1)
        
        if showTitle:
            # plt.title("Precision-Recall Curve \n PSNR = " + str(snrValue))
            plt.title("PSNR = " + str(snrValue))
        plt.xlabel("Recall")
        plt.ylabel("Precision")
        
        pltLegend = []
        
        
        if plotOriginal:
            x = df[self.KEYWORDS_PLOT_RECALL_CLASSIC]
            y = df[self.KEYWORDS_PLOT_PRECISION_CLASSIC]
            

            # newDf[[self.KEYWORDS_PLOT_RECALL_CLASSIC, self.KEYWORDS_PLOT_PRECISION_CLASSIC, self.KEYWORDS_THRESHOLD]].plot.scatter(x = self.KEYWORDS_PLOT_RECALL_CLASSIC, y = self.KEYWORDS_PLOT_PRECISION_CLASSIC, c = self.KEYWORDS_THRESHOLD, marker = '_', cmap='viridis', ax = ax)
            plt.plot(x, y, linewidth = 1.0, color = PEARSON_CLASSIC_COLOR)
            pltLegend.append("PCI")
        
        if plotModified:
            x = df[self.KEYWORDS_PLOT_RECALL_MODIFIED]
            y = df[self.KEYWORDS_PLOT_PRECISION_MODIFIED]

            plt.plot(x, y, linewidth = 1.0, color = PEARSON_MODIFIED_COLOR)
            pltLegend.append("SPCI")
            
        if plotSignal:
            x = df[self.KEYWORDS_PLOT_RECALL_SIGNAL]
            y = df[self.KEYWORDS_PLOT_PRECISION_SIGNAL]

            plt.plot(x, y, linewidth = 1.0, color = SIGNAL_COLOR)
            pltLegend.append("CLT")
        
        
        plt.legend(pltLegend, loc = 'lower left', fontsize = DEFAULT_FONT_SIZE - 1)
        plt.ylim([0, 1.05])
        plt.xlim([0, 1.05])
        
        plt.axhline(y = 1.0, color = 'gray', linewidth = 0.5, linestyle = '--')
        plt.axvline(x = 1.0, color = 'gray', linewidth = 0.5, linestyle = '--')
        
        axes=plt.gca()
        axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)
        

        plt.savefig(currentDir + '/scripts/plots/results_' + outputFileName, bbox_inches = 'tight')
        plt.close()
        
        setFontSize(self.fontSize)

        
    
    
    
    # Plot PR-AUC sweep for selected SNR and threshold ranges
    def plotAUCPR_sweep(self, aucDf, plotOriginal = True,
                        plotModified = True, plotSignal = True,
                        thresholdLimits = False, snrLimits = False,
                        pltRatio = 0.8, outputFileName = 'snrSweep_PR_AUC.pdf',
                        interpolate = False):
        
              
        aucDf, thrLims = resultsComputations.computeAUCPR_sweepSNR(plotOriginal = plotOriginal,
                                                  plotModified = plotModified,
                                                  plotSignal = plotSignal,
                                                  thresholdLimits = thresholdLimits,
                                                  snrLimits = snrLimits,
                                                  returnThresholdLimits = True)
        
        thresholdLimits = thrLims
        
        if thresholdLimits == False:
            thresholdLimits = ()

        setFontSize(self.fontSize - 0)
        
        # plt.title("Precision-Recall AUC \n Threshold range = " + str(thresholdLimits))
        plt.title("Precision-Recall AUC")
        plt.xlabel("PSNR")
        plt.ylabel("PR Area under curve")
        
        pltLegend = []
        
        
        
        if plotOriginal:
            x = aucDf[self.KEYWORDS_SNR]
            y = aucDf[self.KEYWORDS_AUC_PR_CLASSIC]
            if interpolate:
                (x,y) = self.auxComputation.interpolateCurve(x, y, int(interpolate))
          
            plt.plot(x, y, linewidth = 0.75, color = PEARSON_CLASSIC_COLOR)
            pltLegend.append("PCI")
        
        if plotModified:
            x = aucDf[self.KEYWORDS_SNR]
            y = aucDf[self.KEYWORDS_AUC_PR_MODIFIED]
            if interpolate:
                (x,y) = self.auxComputation.interpolateCurve(x, y, int(interpolate))
            
            plt.plot(x, y, linewidth = 0.75, color = PEARSON_MODIFIED_COLOR)
            pltLegend.append("SPCI")
            
        if plotSignal:
            x = aucDf[self.KEYWORDS_SNR]
            y = aucDf[self.KEYWORDS_AUC_PR_SIGNAL]
            if interpolate:
                (x,y) = self.auxComputation.interpolateCurve(x, y, int(interpolate))
            
            
            plt.plot(x, y, linewidth = 0.75, color = SIGNAL_COLOR)
            pltLegend.append("CLT")
            
            outputFileName = outputFileName.rstrip(".pdf") + "_withSignal.pdf"
        
        
        plt.legend(pltLegend, loc = 'lower right', fontsize = DEFAULT_FONT_SIZE - 1)
        plt.ylim([0, 1.05])
        plt.xlim((aucDf[self.KEYWORDS_SNR].min() - 0.25, aucDf[self.KEYWORDS_SNR].max() + 0.25))
        
        plt.axhline(y = 1.0, color = 'gray', linewidth = 0.5, linestyle = '--')
        
        axes=plt.gca()
        axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)
        
        axes.grid(which='major', axis='both', linestyle='--', linewidth = 0.35)

        plt.savefig(currentDir + '/scripts/plots/results_' + outputFileName, bbox_inches = 'tight')
        plt.close()
        
        setFontSize(self.fontSize)


    # Plot the simple trigger count value to compare raw signal and PCI in threshold sweep (used in Section 2)
    def plotThresholdSweepFixedSNR_TrigCount(self, snrValue, plotOriginal = True,
                                       plotModified = False, plotSignal = True,
                                       thresholdLimits = None, pltRatio = 0.8,
                                       outputFileName = 'thresholdSweepFixedSNR_TrigCount.pdf',
                                       interpolate = False):
        if not thresholdLimits:
            df = self.df.loc[self.df[self.KEYWORDS_SNR] == snrValue]
        else:
            df = self.df.loc[((self.df[self.KEYWORDS_SNR] == snrValue) & (self.df[self.KEYWORDS_THRESHOLD] >= thresholdLimits[0]) & (self.df[self.KEYWORDS_THRESHOLD] <= thresholdLimits[1]))]
            plt.xlim(thresholdLimits)
        
        newDf = pd.DataFrame()
        
        newDf = df[[self.KEYWORDS_SNR, self.KEYWORDS_THRESHOLD]]
        
        setFontSize(self.fontSize - 0)
        
        plt.title("Triggered events vs. Threshold value")
        plt.xlabel("Threshold level")
        plt.ylabel("Trigger counts")
        
        pltLegend = []
        
          
        
        if plotOriginal:
            x = newDf[self.KEYWORDS_THRESHOLD]  
            y = df[self.KEYWORDS_TRIGGERCOUNT_CLASSIC]
            if interpolate:
                (x, y) = self.auxComputation.interpolateCurve(x, y, interpolate)
            
            plt.plot(x, y, linewidth = 1.0, color = PEARSON_CLASSIC_COLOR)
            pltLegend.append("Pearson")
        
        if plotModified:
            x = newDf[self.KEYWORDS_THRESHOLD]  
            y = df[self.KEYWORDS_TRIGGERCOUNT_MODIFIED]
            if interpolate:
                (x, y) = self.auxComputation.interpolateCurve(x, y, interpolate)
                
            plt.plot(x, y, linewidth = 1.0, color = PEARSON_MODIFIED_COLOR)
            pltLegend.append("Pearson-like correlation")
            
        if plotSignal:
            x = newDf[self.KEYWORDS_THRESHOLD]  
            y = df[self.KEYWORDS_TRIGGERCOUNT_SIGNAL]
            if interpolate:
                (x, y) = self.auxComputation.interpolateCurve(x, y, interpolate)
                
            plt.plot(x, y, linewidth = 1.0, color = SIGNAL_COLOR)
            pltLegend.append("Signal")
        
        expectedValue = df[self.KEYWORDS_TRIGGERCOUNT_EXPECTED].values[0]
        
        plt.axhline(y = expectedValue, color = 'red', linewidth = 0.75, linestyle = '--')
        pltLegend.append(self.KEYWORDS_TRIGGERCOUNT_EXPECTED)
        
        plt.legend(pltLegend, loc = 'upper right', fontsize = DEFAULT_FONT_SIZE - 1)
        plt.ylim([0, expectedValue*5])
        
        plt.axhline(y = 1.0, color = 'gray', linewidth = 0.5, linestyle = '--')
        
        axes=plt.gca()
        axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)

        plt.savefig(currentDir + '/scripts/plots/results_' + outputFileName, bbox_inches = 'tight')
        plt.close()
        
        setFontSize(self.fontSize)



'''
==========================================================================================
                    Hardware implementation inherited plot class
==========================================================================================
'''

class HardwareResults(Results):
    def __init__(self, hlsresultsFile, fontSize, currentDir = './',
                    differencePlotsYLims = (-0.5, 0.5)):
        super().__init__(resultsFile = hlsresultsFile,
                         fontSize = fontSize,
                         currentDir = currentDir)
        self.auxComputation = ResultsComputations()
        
        self.HW_COLORS = {
            self.KEYWORDS_TRACE_CLASSIC_SIM : 'tab:blue',
            self.KEYWORDS_TRACE_CLASSIC_HW : 'darkorange',
            self.KEYWORDS_TRACE_CLASSIC_DIFF : 'red',
            self.KEYWORDS_TRACE_MODIFIED_SIM : 'darkgreen',
            self.KEYWORDS_TRACE_MODIFIED_HW  : 'tab:purple',
            self.KEYWORDS_TRACE_MODIFIED_DIFF : 'grey'
        }

        self.DIFF_Y_LIMS = differencePlotsYLims
        

    # Plot comparison of PCI for Simulation and HLS results
    def plotClassicSimVsHw(self, outputFileName = 'hwResultsClassicSimVsClassicHw.pdf', xLims = None, pltRatio = 0.8, showAndSave = True):
        tracesNames = ["PCI Simulation", "PCI Hardware"]
        self.__plotTracesHwResults(tracesKeywordsList = [self.KEYWORDS_TRACE_CLASSIC_SIM, self.KEYWORDS_TRACE_CLASSIC_HW],
                                   tracesNamesList = tracesNames,
                                   outputFileName = outputFileName,
                                   xLims = xLims,
                                   pltRatio = pltRatio,
                                   showAndSave = showAndSave)

    # Plot the difference (sample by sample) of PCI (Simulation - HLS)
    def plotClassicDiff(self, outputFileName = 'hwResultsClassicDiff.pdf', xLims = None, pltRatio = 0.8, showAndSave = True):
        tracesNames = ["PCI difference"]
        tracesNames = []

        # Some computation must be carried out to determine the sample-by-sample difference
        thisDf = self.__computeTraceDifferences(originDataSet = self.df,
                                        minuendKeyword = self.KEYWORDS_TRACE_CLASSIC_SIM,
                                        sustraendKeyword = self.KEYWORDS_TRACE_CLASSIC_HW,
                                        resultsKeyword = self.KEYWORDS_TRACE_CLASSIC_DIFF)

        self.__plotTracesHwResults(tracesKeywordsList = [self.KEYWORDS_TRACE_CLASSIC_DIFF,],
                                   tracesNamesList = tracesNames,
                                   outputFileName = outputFileName,
                                   xLims = xLims,
                                   pltRatio = pltRatio / 2,
                                   showAndSave = showAndSave,
                                   yLims = self.DIFF_Y_LIMS,
                                   useAlternativeDataset = True,
                                   alternativeDataset = thisDf,
                                   alternativeYAxisText = "Index difference")
  
        
    # Plot comparison of SPCI for Simulation and HLS results    
    def plotModifiedSimVsHw(self, outputFileName = 'hwResultsModifiedSimVsHw.pdf', xLims = None, pltRatio = 0.8, showAndSave = True):
        tracesNames = ["SPCI Simulation", "SPCI Hardware"]
        self.__plotTracesHwResults(tracesKeywordsList = [self.KEYWORDS_TRACE_MODIFIED_SIM, self.KEYWORDS_TRACE_MODIFIED_HW],
                                   tracesNamesList = tracesNames,
                                   outputFileName = outputFileName,
                                   xLims = xLims,
                                   pltRatio = pltRatio,
                                   showAndSave = showAndSave)

    # Plot the difference (sample by sample) of SPCI (Simulation - HLS)
    def plotModifiedDiff(self, outputFileName = 'hwResultsModifiedDiff.pdf', xLims = None, pltRatio = 0.8, showAndSave = True):
        tracesNames = ["SPCI difference"]
        tracesNames = []

        # Some computation must be carried out to determine the sample-by-sample difference
        thisDf = self.__computeTraceDifferences(originDataSet = self.df,
                                        minuendKeyword = self.KEYWORDS_TRACE_MODIFIED_SIM,
                                        sustraendKeyword = self.KEYWORDS_TRACE_MODIFIED_HW,
                                        resultsKeyword = self.KEYWORDS_TRACE_MODIFIED_DIFF)

        self.__plotTracesHwResults(tracesKeywordsList = [self.KEYWORDS_TRACE_MODIFIED_DIFF,],
                                   tracesNamesList = tracesNames,
                                   outputFileName = outputFileName,
                                   xLims = xLims,
                                   pltRatio = pltRatio / 2,
                                   showAndSave = showAndSave,
                                   yLims = self.DIFF_Y_LIMS,
                                   useAlternativeDataset = True,
                                   alternativeDataset = thisDf,
                                   alternativeYAxisText = "Index difference")                               
    
    # Plot comparison of PCI Simulation with and PCI and SPCI HLS results    
    def plotClassicSimVsClassicAndModifiedHw(self, outputFileName = 'hwResultsPCCSim_SPCCsimAndHw.pdf', xLims = None, pltRatio = 0.8, showAndSave = True):
        tracesNames = ["PCI Simulation", "PCI Hardware", "SPCI Hardware"]
        tracesKeywords = [self.KEYWORDS_TRACE_CLASSIC_SIM, self.KEYWORDS_TRACE_CLASSIC_HW,
                          self.KEYWORDS_TRACE_MODIFIED_HW]
        self.__plotTracesHwResults(tracesKeywordsList = tracesKeywords,
                                   tracesNamesList = tracesNames,
                                   outputFileName = outputFileName,
                                   xLims = xLims,
                                   pltRatio = pltRatio,
                                   showAndSave = showAndSave) 
    
    
    # Plot comparison of PCI and SPCI for Simulation and HLS results    
    def plotAll(self, outputFileName = 'hwResultsAll.pdf', xLims = None, pltRatio = 0.8, showAndSave = True):
        tracesNames = ["PCI Simulation", "PCI Hardware", "SPCI Simulation", "SPCI Hardware"]
        tracesKeywords = [self.KEYWORDS_TRACE_CLASSIC_SIM, self.KEYWORDS_TRACE_CLASSIC_HW,
                          self.KEYWORDS_TRACE_MODIFIED_SIM, self.KEYWORDS_TRACE_MODIFIED_HW]
        self.__plotTracesHwResults(tracesKeywordsList = tracesKeywords,
                                   tracesNamesList = tracesNames,
                                   outputFileName = outputFileName,
                                   xLims = xLims,
                                   pltRatio = pltRatio,
                                   showAndSave = showAndSave)
    

    # Plot both differences (sample by sample) of PCI & SPCI (Simulation - HLS) in the same plot
    def plotClassicAndModifiedDiff(self, outputFileName = 'hwResultsClassicAndModifiedDiff.pdf', xLims = None, pltRatio = 0.8, showAndSave = True):
        tracesNames = ["PCI difference", "SPCI difference"]

        # Some computation must be carried out to determine the sample-by-sample difference
        # First, compute the PCI differences
        thisDf = self.__computeTraceDifferences(originDataSet = self.df,
                                        minuendKeyword = self.KEYWORDS_TRACE_CLASSIC_SIM,
                                        sustraendKeyword = self.KEYWORDS_TRACE_CLASSIC_HW,
                                        resultsKeyword = self.KEYWORDS_TRACE_CLASSIC_DIFF)


        # Then take the previous output and append the SPCI differences (originDataSet now is thisDf)
        thisDf = self.__computeTraceDifferences(originDataSet = thisDf,
                                        minuendKeyword = self.KEYWORDS_TRACE_MODIFIED_SIM,
                                        sustraendKeyword = self.KEYWORDS_TRACE_MODIFIED_HW,
                                        resultsKeyword = self.KEYWORDS_TRACE_MODIFIED_DIFF)
        
        

        self.__plotTracesHwResults(tracesKeywordsList = [self. KEYWORDS_TRACE_CLASSIC_DIFF, self.KEYWORDS_TRACE_MODIFIED_DIFF],
                                   tracesNamesList = tracesNames,
                                   outputFileName = outputFileName,
                                   xLims = xLims,
                                   pltRatio = pltRatio / 2,
                                   showAndSave = showAndSave,
                                   yLims = self.DIFF_Y_LIMS,
                                   useAlternativeDataset = True,
                                   alternativeDataset = thisDf,
                                   alternativeYAxisText = "Index difference") 
        

    def stackSubPlots(self, plotsList, pltRatio = 0.3, fileName = 'hwResults_stacked.pdf'):
        from matplotlib import gridspec
        plt.figure()
        fig = gridspec.GridSpec(len(plotsList), 1, height_ratios = [1,]*len(plotsList))
        
        for i in range(len(plotsList)):
            x = plt.subplot(fig[i])
            plotsList[i](showAndSave = False)
            axes=plt.gca()
            axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)
        
        
        
        plt.savefig(currentDir + '/scripts/plots/results_' + fileName, bbox_inches = 'tight')
        plt.close()
        setFontSize(self.fontSize)
    

    # Generic plotting method for traces. Not available out of this class
    def __plotTracesHwResults(self, tracesKeywordsList, tracesNamesList, outputFileName,
                            xLims = None, pltRatio = 0.8, showAndSave = True, yLims= None,
                            useAlternativeDataset = None, alternativeDataset = None,
                            alternativeYAxisText = None):
        
        # In case a pre-computed dataset is required instead of loading data directly from file
        if useAlternativeDataset:
            thisDf = alternativeDataset
        else:
            thisDf = self.df
        
        
        setFontSize(self.fontSize - 0)
        
        traceLen = thisDf[self.KEYWORDS_TRACE_CLASSIC_HW].shape[0]
        
        x = np.linspace(0, traceLen, traceLen)
        
        if len(tracesKeywordsList):
            
            plt.xlabel("Time (samples)")
            if alternativeYAxisText:
                plt.ylabel(alternativeYAxisText)
            else:
                plt.ylabel("Correlation index")
            
            for trace in tracesKeywordsList:
                plt.plot(x, thisDf[trace][:traceLen], color = self.HW_COLORS[trace], linewidth = 0.75)
                # plt.plot(x, thisDf[trace][:traceLen], linewidth = 0.75)
            
            if len(tracesNamesList):
                plt.legend(tracesNamesList, loc = 'lower left', fontsize = DEFAULT_FONT_SIZE - 1 - len(tracesNamesList))
            plt.axhline(y = 0.0, color = 'gray', linewidth = 0.5, linestyle = '--')
            plt.axhline(y = 1.0, color = 'gray', linewidth = 0.5, linestyle = '--')
            plt.axhline(y = -1.0, color = 'gray', linewidth = 0.5, linestyle = '--')
            plt.ylim([-1.05, 1.05])
            if xLims:
                plt.xlim(xLims)

            if yLims:
                plt.ylim(yLims)
            
            axes=plt.gca()
            axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)
            
            if showAndSave:
                plt.savefig(currentDir + '/scripts/plots/results_' + outputFileName, bbox_inches = 'tight')
                plt.close()
            

    '''
    Computes the difference trace between two results for sake of easier comparison
    Parameters: 
        - Original dataset to take the data from
        - Minuend column KEYWORD
        - Sustraend column KEYWORD
        - Results column KEYWORD
    
    Returns: 
        - Dataset equal to original with added column
    '''
    def __computeTraceDifferences(self, originDataSet, minuendKeyword,
                                    sustraendKeyword, resultsKeyword):
        outputDf = originDataSet.copy()
        outputDf[resultsKeyword] = outputDf[minuendKeyword] - outputDf[sustraendKeyword]

        return outputDf
    
'''
==========================================================================================
                        Results-related inherited computations class
==========================================================================================
'''
class ResultsComputations(Results):
    def __init__(self, df = False):
        self.df = df
        super().__init__(resultsFile = False, fontSize = False)
        
    # Compute the area under the curve for a fixed SNR value
    def computeAUCPR_FixedSNR(self, snrValue, plotOriginal = True,
                                plotModified = True, plotSignal = True,
                                thresholdLimits = None):
        
        if not thresholdLimits:
            df = self.df.loc[self.df[self.KEYWORDS_SNR] == snrValue]
        else:
            df = self.df.loc[((self.df[self.KEYWORDS_SNR] == snrValue) & (self.df[self.KEYWORDS_THRESHOLD] >= thresholdLimits[0]) & (self.df[self.KEYWORDS_THRESHOLD] <= thresholdLimits[1]))]
        
        newDf = pd.DataFrame()
        newDf = df[[self.KEYWORDS_SNR, self.KEYWORDS_THRESHOLD]]
        
        aucResults = {self.KEYWORDS_SNR: snrValue}
        
        if plotOriginal:
            newDf[self.KEYWORDS_PLOT_RECALL_CLASSIC] = df[self.KEYWORDS_PLOT_RECALL_CLASSIC]
            newDf[self.KEYWORDS_PLOT_PRECISION_CLASSIC] = df[self.KEYWORDS_PLOT_PRECISION_CLASSIC]
            thisAuc = self.computeAUC(newDf[self.KEYWORDS_PLOT_RECALL_CLASSIC], newDf[self.KEYWORDS_PLOT_PRECISION_CLASSIC])
            aucResults[self.KEYWORDS_AUC_PR_CLASSIC] = thisAuc

        
        if plotModified:
            newDf[self.KEYWORDS_PLOT_RECALL_MODIFIED] = df[self.KEYWORDS_PLOT_RECALL_MODIFIED]
            newDf[self.KEYWORDS_PLOT_PRECISION_MODIFIED] = df[self.KEYWORDS_PLOT_PRECISION_MODIFIED]
            thisAuc  = self.computeAUC(newDf[self.KEYWORDS_PLOT_RECALL_MODIFIED], newDf[self.KEYWORDS_PLOT_PRECISION_MODIFIED])
            aucResults[self.KEYWORDS_AUC_PR_MODIFIED] = thisAuc

           
            
        if plotSignal:
            newDf[self.KEYWORDS_PLOT_RECALL_SIGNAL] = df[self.KEYWORDS_PLOT_RECALL_SIGNAL]
            newDf[self.KEYWORDS_PLOT_PRECISION_SIGNAL] = df[self.KEYWORDS_PLOT_PRECISION_SIGNAL]
            thisAuc = self.computeAUC(newDf[self.KEYWORDS_PLOT_RECALL_SIGNAL], newDf[self.KEYWORDS_PLOT_PRECISION_SIGNAL])
            aucResults[self.KEYWORDS_AUC_PR_SIGNAL] = thisAuc

        return aucResults
        
    
    # Simple area-under-curve computation with simpsons rule
    def computeAUC(self, x, y):
        x = np.array(x)
        y = np.array(y)
        area = 0

        for i in range(len(x) - 1):
            if np.isnan(y[i]):
                y[i] = 0
            
            if np.isnan(y[i + 1]):
                y[i + 1] = 0
            # area += np.abs(x[i] - x[i + 1]) * (y[i] + y[i + 1]) / 2
            area += (x[i] - x[i + 1]) * (y[i] + y[i + 1]) / 2
            # area += np.abs(x[i + 1] - x[i]) * (y[i] + 4 * y[i + 1] + y[i + 2]) / 6
            
        return area
    
    
    # Compute the area under the curves for all SNR values
    def computeAUCPR_sweepSNR(self, plotOriginal = True,
                                plotModified = True, plotSignal = True,
                                thresholdLimits = None,
                                snrLimits = None, returnThresholdLimits = False):

        if not thresholdLimits:
            thresholdLimits = []
            thresholdLimits.append(self.df[self.KEYWORDS_THRESHOLD].min())
            thresholdLimits.append(self.df[self.KEYWORDS_THRESHOLD].max())
            if not snrLimits:
                df = self.df
                snrLimits = []
                snrLimits.append(self.df[self.KEYWORDS_SNR].min())
                snrLimits.append(self.df[self.KEYWORDS_SNR].max())
            else:
                df = self.df.loc[(self.df[self.KEYWORDS_SNR] >= snrLimits[0]) & (self.df[self.KEYWORDS_SNR] <= snrLimits[1])]
        else:
            if not snrLimits:
                snrLimits = []
                snrLimits.append(self.df[self.KEYWORDS_SNR].min())
                snrLimits.append(self.df[self.KEYWORDS_SNR].max())
                df = self.df.loc[(self.df[self.KEYWORDS_THRESHOLD] >= thresholdLimits[0]) & (self.df[self.KEYWORDS_THRESHOLD] <= thresholdLimits[1])]
            else:
                df = self.df.loc[(self.df[self.KEYWORDS_THRESHOLD] >= thresholdLimits[0]) & (self.df[self.KEYWORDS_THRESHOLD] <= thresholdLimits[1]) & (self.df[self.KEYWORDS_SNR] >= snrLimits[0]) & (self.df[self.KEYWORDS_SNR] <= snrLimits[1])]
            
        aucDf = pd.DataFrame()

        # Retrieve the individual SNR values
        snrValues = df[self.KEYWORDS_SNR].unique()
        
        thisAuc = {}
        
        for i in snrValues:
            thisAuc = self.computeAUCPR_FixedSNR(snrValue = i,
                                                 plotOriginal = plotOriginal,
                                                 plotModified = plotModified,
                                                 plotSignal = plotSignal,
                                                 thresholdLimits = thresholdLimits)            
            
            aucDf = aucDf.append(thisAuc, ignore_index = True)
        
        if returnThresholdLimits:
            return (aucDf, thresholdLimits)
            
        return aucDf
    
    def interpolateCurve(self, xData, yData, nPoints, interpolationMethod = 'cubic'):
        
        from scipy.interpolate import interp1d

        # Define new x-axis 
        xNew = np.linspace(min(xData), max(xData), num=nPoints, endpoint=True)

        # Interpolate the data
        interpolatedFunction = interp1d(xData, yData, kind = interpolationMethod)
        
        # Evaluate the new interpolated x-axis with the interpolated function
        yNew = interpolatedFunction(xNew)
        

        return (xNew, yNew)
    
   
   
            
if __name__ == '__main__':


    cc = numericPearson.CorrelationComparison()
    signal = PulseSynthesis()
    
    TEST_PULSE_LEN = 64
    PULSE_TYPE = signal.pmtPulse
    SIGNAL_TO_NOISE_RATIO = 3
    THRESHOLD_SIGMAS = 1
    ORIGINAL_PULSE_REPLICAS = 10
    LEADING_TO_LENGTH_RATIO = 0.45
    DISTANCE_BETWEEN_PULSES = [TEST_PULSE_LEN * 0.05 , TEST_PULSE_LEN * 2]
    LOW_FREQ_NOISE_INDEX = 0.00
    PILE_UP_BETA = 1.75
    
    #DO NOT MODIFY THESE CONSTANTS
    # NOISE_AMPLITUDE = signal.snrToSigma(SIGNAL_TO_NOISE_RATIO, MAX_PULSE_AMPLITUDE)
    
    
    # Ideal pulse used to generate coefficients
    c = PULSE_TYPE(
            length = TEST_PULSE_LEN,
            amplitude = 1,
            noiseSigma = 0, #Ideal pulse means NO NOISE
            leadingProportion = LEADING_TO_LENGTH_RATIO
        )
    
    
    
    
    NOISE_AMPLITUDE = 1 # Setting noise sigma = 1
    MAX_PULSE_AMPLITUDE = signal.snrToSignalAmp(snr = SIGNAL_TO_NOISE_RATIO)
    # MAX_PULSE_AMPLITUDE = signal.snrToSignalAmp(snr = SIGNAL_TO_NOISE_RATIO, patternPulse = c)
    MIN_PULSE_AMPLITUDE = MAX_PULSE_AMPLITUDE #Equal pulse amplitudes


    #Visual line value to show "valid" correlation threshold
    CORRELATION_THRESHOLD_LINE = round((1 / SIGNAL_TO_NOISE_RATIO) * THRESHOLD_SIGMAS, 4)


    # Noisy synthetic signal
    x, xPos, xNoiseless = signal.pmtTrace(
        nPulses = ORIGINAL_PULSE_REPLICAS,
        pulseLength = TEST_PULSE_LEN,
        pulseType = PULSE_TYPE,
        leadingProportion =  LEADING_TO_LENGTH_RATIO,
        minAmp=MIN_PULSE_AMPLITUDE,
        maxAmp=MAX_PULSE_AMPLITUDE,
        traceNoiseSigma=NOISE_AMPLITUDE,
        lowFreqNoiseContribution=LOW_FREQ_NOISE_INDEX,
        minSeparation = DISTANCE_BETWEEN_PULSES[0],
        maxSeparation = DISTANCE_BETWEEN_PULSES[1],
        returnPulseLocations = True,
        plotNoiseDist = False,
        # amplitudeFromSNR = SIGNAL_TO_NOISE_RATIO,
        amplitudeFromSNR = False,
        returnNoiselessTrace = True,
        leadingZeroes = True,
        trailingZeroes = True
    )
    
    numPulses = len(xPos)
    
    # Noisy synthetic signal with pile-up
    xP, xPosP, xNoiselessP = signal.pmtTraceWithPileup(
        nPulses = ORIGINAL_PULSE_REPLICAS,
        pulseLength = TEST_PULSE_LEN,
        pulseType = PULSE_TYPE,
        leadingProportion =  LEADING_TO_LENGTH_RATIO,
        minAmp=MIN_PULSE_AMPLITUDE,
        maxAmp=MAX_PULSE_AMPLITUDE,
        traceNoiseSigma=NOISE_AMPLITUDE,
        lowFreqNoiseContribution=LOW_FREQ_NOISE_INDEX,
        normalizedBeta = PILE_UP_BETA,
        returnPulseLocations = True,
        plotNoiseDist = False,
        # amplitudeFromSNR = SIGNAL_TO_NOISE_RATIO,
        amplitudeFromSNR = False,
        returnNoiselessTrace = True,
        leadingZeroes = True,
        trailingZeroes = True
    )
    
    numPulsesP = len(xPosP)
    
    
    '''
    ================================
    Sliding window correlation tests
    ================================
    '''

    cTests = numericPearson.CorrelationTests(thresholdLimits = [0.00, 1.75],
                              thresholdStepSize = 0.025,
                              snrLimits = [1, 10],
                              snrStepSize = 0.25,
                              nPulses = ORIGINAL_PULSE_REPLICAS  
                              )

    thisCorrelations = cTests.computeSingleCorrelations(x, c, plot = False, signalAmplitude = MAX_PULSE_AMPLITUDE, correlationThresholdLine = CORRELATION_THRESHOLD_LINE)
  
    
    '''
    =========================
    Pattern coefficients plot
    =========================
    '''
    
    def plotPatternCoefficients(pltRatio = 0.8):
            
        t = np.arange(len(c))

        setFontSize(8)

        #Plotting self-correlation results
        plt.plot(t, c, color = 'red', marker = '.', linewidth = 0.75)
        plt.title(r'Pattern signal', fontsize = 14)
        plt.xlabel(r'Time (samples)', fontsize = 12, labelpad = 5)
        plt.ylabel(r'Amplitude (arbitrary units)', fontsize = 12)
        axes=plt.gca()
        axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)
        plt.savefig(currentDir + '/scripts/plots/patternCoefficients.pdf', bbox_inches = 'tight')
        plt.close()
        
        setFontSize()

    
    
    
    
    
    '''
    ===================
    Pattern signal plot
    ===================
    '''
    
    def plotPatternSignal(pltRatio = 0.8):
            
        t = np.arange(len(c))
        
        setFontSize(8)

        #Plotting self-correlation results
        plt.plot(t, c, label = "Synthetic pulse", color = 'red', marker = ".", linewidth = 0.75)
        # plt.suptitle(r'Pattern signal ($c$)', fontsize = 14)
        plt.title(r'Pattern signal ($c$)')
        plt.xlabel(r'Time (samples)', fontsize = 12, labelpad = 5)
        plt.ylabel(r'Amplitude (arbitrary units)', fontsize = 12)
        axes=plt.gca()
        axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)
        # plt.legend(["Double-exponential model", ])
        plt.savefig(currentDir + '/scripts/plots/patternSignal.pdf', bbox_inches = 'tight')
        plt.close()
        
        setFontSize()
        
      
    '''
    ===========================
    Noiseless signal trace plot
    ===========================
    '''  
    def plotNoiselessTrace(pltRatio = 0.5):
        
        setFontSize(8)
        
        plt.figure(1)
        
        plt.plot(xNoiseless[:len(xNoiseless) - len(c)], '-', linewidth = 0.9, markeredgewidth = 0.1)
        plt.axhline(y = 0, color = 'gray', linewidth = 0.4, linestyle = '--')
        plt.title("Original noiseless trace ($\\vec{w}$)")
        plt.xlabel("Time (samples)")
        plt.ylabel(r"Amplitude (a. u.)")
        axes=plt.gca()
        axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)
        plt.savefig(currentDir + '/scripts/plots/noiselessTrace.pdf', bbox_inches = 'tight')
        plt.close()
        
        setFontSize()
        
        
    '''
    ========================================
    Noiseless signal trace plot with pile-up
    ========================================
    '''  
    def plotNoiselessTracePileUp(pltRatio = 0.5):
        
        setFontSize(8)
        
        plt.figure(1)
        plt.plot(xNoiselessP[:len(xNoiselessP) - len(c)], '-', linewidth = 0.9, markeredgewidth = 0.1)
        plt.axhline(y = 0, color = 'gray', linewidth = 0.4, linestyle = '--')
        plt.title("Original noiseless trace ($w$)")
        plt.xlabel("Time (samples)")
        plt.ylabel(r"Amplitude (a. u.)")
        axes=plt.gca()
        axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)
        plt.savefig(currentDir + '/scripts/plots/noiselessTraceWithPileUp.pdf', bbox_inches = 'tight')
        plt.close()
        
        setFontSize(8)
    
    
    
    '''
    ===============================
    Synthetic signal with threshold
    ===============================
    '''
    
    def plotStimulusWithThreshold(pltRatio = 0.5, thresholdLevel = 0):
        plt.figure(1)
        plt.plot(x[:len(x) - len(c)], '-', linewidth = 0.5, markeredgewidth = 0.1)
        plt.axhline(y = thresholdLevel, color = PEARSON_MODIFIED_COLOR, linewidth = 1.0, linestyle = '--')
        plt.axhline(y = 0, color = 'gray', linewidth = 0.4, linestyle = '--')
        plt.title("Input trace ($x$)")
        plt.xlabel("Time (samples)")
        plt.ylabel(r"Amplitude (a. u.)")
        plt.legend(["Input trace", "Threshold"], loc = 'lower right', fontsize = DEFAULT_FONT_SIZE - 1)
        axes=plt.gca()
        axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)
        plt.savefig(currentDir + '/scripts/plots/syntheticSignalWithThreshold.pdf', bbox_inches = 'tight')
        plt.close()
    
    
    
    '''
    ============================================
    Synthetic signal with threshold with pile-up
    ============================================
    '''
    
    def plotStimulusWithThresholdPileUp(pltRatio = 0.5, thresholdLevel = 0):
        plt.figure(1)
        plt.plot(xP[:len(xP) - len(c)], '-', linewidth = 0.5, markeredgewidth = 0.1)
        plt.axhline(y = thresholdLevel, color = PEARSON_MODIFIED_COLOR, linewidth = 1.0, linestyle = '--')
        plt.axhline(y = 0, color = 'gray', linewidth = 0.4, linestyle = '--')
        plt.title("Input trace ($x$)")
        plt.xlabel("Time (samples)")
        plt.ylabel(r"Amplitude (a. u.)")
        plt.legend(["Input trace", "Threshold"], loc = 'lower right', fontsize = DEFAULT_FONT_SIZE - 1)
        axes=plt.gca()
        axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)
        plt.savefig(currentDir + '/scripts/plots/syntheticSignalWithThresholdWithPileUp.pdf', bbox_inches = 'tight')
        plt.close()
    
    
    
    
    '''
    ===========================
    Synthetic signal trace plot
    ===========================
    '''
    
    def plotSyntheticTrace(pltRatio = 0.5, snr = 3):
        plt.figure(1)
        plt.plot(x[:len(x) - len(c)], '-', linewidth = 0.5, markeredgewidth = 0.1)
        plt.axhline(y = 0, color = 'gray', linewidth = 0.4, linestyle = '--')
        plt.title("Input signal ($x$)")
        plt.xlabel("Time (samples)")
        plt.ylabel(r"Amplitude (a. u.)")
        if snr:
            plt.legend(["PSNR = " + str(snr)], loc = 'upper right', fontsize = DEFAULT_FONT_SIZE - 2)
        axes=plt.gca()
        axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)
        plt.savefig(currentDir + '/scripts/plots/syntheticSignal.pdf', bbox_inches = 'tight')
        plt.close()
        
        
        
    '''
    ========================================
    Synthetic signal trace plot with pile-up
    ========================================
    '''
    
    def plotSyntheticTracePileUp(pltRatio = 0.5, snr = 3):
        plt.figure(1)
        plt.plot(xP[:len(xP) - len(c)], '-', linewidth = 0.5, markeredgewidth = 0.1)
        plt.axhline(y = 0, color = 'gray', linewidth = 0.4, linestyle = '--')
        plt.title("Input signal ($x$)")
        plt.xlabel("Time (samples)")
        plt.ylabel(r"Amplitude (a. u.)")
        if snr:
            plt.legend(["PSNR = " + str(snr)], loc = 'upper right', fontsize = DEFAULT_FONT_SIZE - 2)
        axes=plt.gca()
        axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)
        plt.savefig(currentDir + '/scripts/plots/syntheticSignalWithPileUp.pdf', bbox_inches = 'tight')
        plt.close()


    
    '''
    =================================================
    Pearson Correlation (Classic) Plot with threshold
    =================================================
    '''
    def plotCorrelationClassicWithThreshold(x, c, pltRatio = 0.3, correlationThresholdLine = 0.5, isPileUp = False):
        
        cc = numericPearson.CorrelationComparison()
        
        __pearsonClassic = cc.correlate(x = x, c = c, algorithm = cc.pearsonClassic)       
        xAxis = np.arange(len(__pearsonClassic) - len(c))
                        
            
        setFontSize(8)

        # Pearson classic plot
        plt.plot(xAxis, __pearsonClassic[:len(xAxis)], linewidth = 0.5, color = PEARSON_CLASSIC_COLOR)               
        # plt.legend(["Pearson correlation", "Simplified correlation"])
        
        # Test correlation threshold                
        if correlationThresholdLine:
            plt.plot(xAxis, [correlationThresholdLine]*len(xAxis), '--', color = PEARSON_MODIFIED_COLOR, linewidth = 0.75)
            plt.legend(["Correlated signal", "Threshold"], loc = 'lower right', fontsize = DEFAULT_FONT_SIZE - 1)
        
        plt.axhline(y = 0, color = 'gray', linewidth = 0.4, linestyle = '--')
        plt.title("Trigger over pre-processed (correlated) input signal", fontsize = 10)
        plt.xlabel("Time (samples)")
        plt.ylabel("Correlation index")
        
        
        axes=plt.gca()
        axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)
        axes.set_ylim([-1, 1])
        filename = currentDir + '/scripts/plots/pccWithThreshold'
        if isPileUp:
            filename += 'WithPileUp'
        
        filename += '.pdf'
        
        plt.savefig(filename, bbox_inches = 'tight')
        plt.close()
        
        setFontSize()
    
    
    
    
    
    '''
    =====================================
    Pearson Correlation (Classic) Plot
    =====================================
    '''
    def plotCorrelationClassic(x, c, pltRatio = 0.3, correlationThresholdLine = False, isPileUp = False):
        
        cc = numericPearson.CorrelationComparison()
        
        __pearsonClassic = cc.correlate(x = x, c = c, algorithm = cc.pearsonClassic)       
        xAxis = np.arange(len(__pearsonClassic) - len(c))
                
        # Test correlation threshold                
        if correlationThresholdLine:
            plt.plot(xAxis, [correlationThresholdLine]*len(xAxis), 'g--', linewidth = 0.75)
            plt.legend(["Correlation threshold", "Pearson correlation index (PCI)"], loc = 'upper right')
        
            
        setFontSize(8)
        
        plt.axhline(y = 0, color = 'gray', linewidth = 0.4, linestyle = '--')
        
        # Pearson classic plot
        plt.plot(xAxis, __pearsonClassic[:len(xAxis)], linewidth = 0.5, color = PEARSON_CLASSIC_COLOR)               
        # plt.legend(["Pearson correlation", "Simplified correlation"])
        
        
        
        plt.title("Pearson correlation index ($\\rho$)", fontsize = 10)
        plt.xlabel("Time (samples)")
        plt.ylabel("Correlation index ($\\rho$)")
        
        
        axes=plt.gca()
        axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)
        axes.set_ylim([-1, 1])
        
        filename = currentDir + '/scripts/plots/pccOnly'
        if isPileUp:
            filename += 'WithPileUp'
            
        filename += '.pdf'
        
        plt.savefig(filename, bbox_inches = 'tight')
        plt.close()
        
        setFontSize()
        
    
    '''
    =====================================
    Pearson Correlation (Simplified) Plot
    =====================================
    '''
    def plotCorrelationSimplified(x, c, pltRatio = 0.3, correlationThresholdLine = False, isPileUp = False):
        
        cc = numericPearson.CorrelationComparison()
        
        __pearsonSimplified = cc.correlate(x = x, c = c, algorithm = cc.pearsonModified)       
        xAxis = np.arange(len(__pearsonSimplified) - len(c))
                
        # Test correlation threshold                
        if correlationThresholdLine:
            plt.plot(xAxis, [correlationThresholdLine]*len(xAxis), 'g--', linewidth = 0.75)
            plt.legend(["Correlation threshold", "Simplified correlation index"], loc = 'upper right')
        
            
        setFontSize(8)
        
        plt.axhline(y = 0, color = 'gray', linewidth = 0.4, linestyle = '--')
        
        # Pearson classic plot
        plt.plot(xAxis, __pearsonSimplified[:len(xAxis)], linewidth = 0.5, color = PEARSON_MODIFIED_COLOR)               
        # plt.legend(["Pearson correlation", "Simplified correlation"])
        
        
        
        plt.title("Simplified Pearson-like correlation ($\\rho'$)", fontsize = 10)
        plt.xlabel("Time (samples)")
        plt.ylabel("Correlation index ($\\rho'$)")
        
        
        axes=plt.gca()
        axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)
        axes.set_ylim([-1, 1])
        
        filename = currentDir + '/scripts/plots/pccSimplifiedOnly'
        
        if isPileUp:
            filename += 'WithPileUp'
            
        filename += '.pdf'
        
        
        plt.savefig(filename, bbox_inches = 'tight')
        plt.close()
        
        setFontSize()
        
    
    '''
    =====================================
    Pearson Correlations (Both) Plot
    =====================================
    '''
    def plotBothCorrelations(x, c, pltRatio = 0.3, correlationThresholdLine = False, isPileUp = False):
        
        cc = numericPearson.CorrelationComparison()
        
        __pearsonClassic = cc.correlate(x = x, c = c, algorithm = cc.pearsonClassic)
        __pearsonSimplified = cc.correlate(x = x, c = c, algorithm = cc.pearsonModified)       
        xAxis = np.arange(len(__pearsonClassic) - len(c))
        
        pltLegend = ["Pearson correlation $\\rho$", "Simplified correlation $\\rho'$"]
                
        # Test correlation threshold                
        if correlationThresholdLine:
            plt.plot(xAxis, [correlationThresholdLine]*len(xAxis), 'g--', linewidth = 0.75)
            pltLegend.insert(0, "Correlation threshold")
        
        
        thisFontSize = 8
         
        setFontSize(thisFontSize)
        
        
        
        # Pearson classic plot
        plt.plot(xAxis, __pearsonClassic[:len(xAxis)], color = PEARSON_CLASSIC_COLOR, linewidth = 0.5)
        plt.plot(xAxis, __pearsonSimplified[:len(xAxis)], linestyle = (0, (5, 5)), markersize = 0.5, color = PEARSON_MODIFIED_COLOR, linewidth = 0.5) 
        plt.legend(pltLegend, loc = 'lower right', fontsize = thisFontSize - 1)
        
        
        
        plt.title("Correlation of input signal ($x$) and pattern ($c$) \n comparison of both indexes ($\\rho$) and ($\\rho'$)", fontsize = 10)
        plt.xlabel("Time (samples)")
        plt.ylabel("Correlation index")
        
        plt.axhline(y = 0, color = 'gray', linewidth = 0.3, linestyle = '--')
        
        axes=plt.gca()
        axes.set_aspect(1.0/axes.get_data_ratio() * pltRatio)
        axes.set_ylim([-1, 1])
        
        filename = currentDir + '/scripts/plots/pearsonBothCorrelations'
        if isPileUp:
            filename += 'WithPileUp'
        
        filename += '.pdf'
        
        
        plt.savefig(filename, bbox_inches = 'tight')
        plt.close()
        
        setFontSize()
        
        
    '''
    ===========================================
    Pearson difference of Correlations Subplots
    ===========================================
    '''
    def plotDifferenceOfCorrelations(x, c, pltRatio = 0.3, correlationThresholdLine = False, isPileUp = False):
        
        cc = numericPearson.CorrelationComparison()
        
        __pearsonClassic = cc.correlate(x = x, c = c, algorithm = cc.pearsonClassic)
        __pearsonSimplified = cc.correlate(x = x, c = c, algorithm = cc.pearsonModified)       
        xAxis = np.arange(len(__pearsonClassic) - len(c))

        
        thisFontSize = 8
         
        setFontSize(thisFontSize)
        
        pltLegend = ["PCI ($\\rho$)", "SPCI ($\\rho'$)"]
        plt.subplot(2,1,1)

        # Test correlation threshold                
        if correlationThresholdLine:
            plt.plot(xAxis, [correlationThresholdLine]*len(xAxis), 'g--', linewidth = 0.75)
            pltLegend.insert(0, "Correlation threshold")
        
        plt.plot(xAxis, __pearsonClassic[:len(xAxis)], color = PEARSON_CLASSIC_COLOR, linewidth = 0.5)
        plt.plot(xAxis, __pearsonSimplified[:len(xAxis)], linestyle = (0, (5, 5)), markersize = 0.5, color = PEARSON_MODIFIED_COLOR, linewidth = 0.5) 
        plt.legend(pltLegend, loc = 'lower right', fontsize = thisFontSize - 1)

        # plt.title("Correlation of input signal ($x$) against pattern ($c}) \n comparison of both indexes ($\\rho$) and ($\\rho'$)", fontsize = 10)
        plt.xlabel("Time (samples)")
        plt.ylabel("Correlation index")
        axes = plt.gca()
        axes.set_ylim([-1, 1])
        plt.axhline(y = 0, color = 'gray', linewidth = 0.3, linestyle = '--')
        
        plt.subplot(2,1,2)
        pltLegend = ["Difference of correlations $\\rho$ - $\\rho'$",]
        # Pearson classic - modified (difference) plot
        plt.plot(xAxis, __pearsonClassic[:len(xAxis)] - __pearsonSimplified[:len(xAxis)], color = CORRELATION_DIFFERENCE_COLOR, linewidth = 0.75)
        # plt.legend(pltLegend, loc = 'lower right', fontsize = thisFontSize - 1)
        
        
        
        # plt.title("Difference of correlations \n $\\rho$ - $\\rho'$", fontsize = 10)
        plt.xlabel("Time (samples)")
        plt.ylabel("PCI - SPCI")
        
        plt.axhline(y = 0, color = 'gray', linewidth = 0.3, linestyle = '--')
        
        axes = plt.gca()
        plt.tight_layout(pad = 2.0)
        axes.set_ylim([-0.12, 0.12])
        # axes.yaxis.get_ticklocs(minor = True)
        # axes.minorticks_on()
        # axes.xaxis.set_tick_params(which = 'minor', bottom = False)
        # plt.grid(b = True, which = 'minor', color = 'gray', linestyle = '--', axis = 'y', linewidth = 0.3)
        
        filename = currentDir + '/scripts/plots/pearsonDifferenceOfCorrelations'
        if isPileUp:
            filename += 'WithPileUp'
        
        filename += '.pdf'
        
        
        plt.savefig(filename, bbox_inches = 'tight')
        plt.close()
        
        setFontSize()





    '''
    ==========================================
    Execute (or comment out) the desired plots
    ==========================================
    '''
    
    '''
    ------------------------------------------------------------
                        Theory-related plots
    ------------------------------------------------------------
    '''
    # plotPatternCoefficients()
    # plotPatternSignal()
    # plotNoiselessTrace()
    # plotNoiselessTracePileUp()
    # plotStimulusWithThreshold(thresholdLevel = 1.0)
    # plotStimulusWithThresholdPileUp(thresholdLevel = 1.0)
    # plotSyntheticTrace(snr = SIGNAL_TO_NOISE_RATIO)
    # plotSyntheticTracePileUp(snr = SIGNAL_TO_NOISE_RATIO)
    # plotCorrelationClassicWithThreshold(x, c, correlationThresholdLine = 0.33)
    # plotCorrelationClassicWithThreshold(xP, c, correlationThresholdLine = 0.33, isPileUp = True)
    # plotCorrelationClassic(x, c)
    # plotCorrelationClassic(xP, c, isPileUp = True)
    # plotCorrelationSimplified(x, c)
    # plotCorrelationSimplified(xP, c, isPileUp = True)
    # plotBothCorrelations(x, c)
    # plotBothCorrelations(xP, c, isPileUp = True)
    # plotDifferenceOfCorrelations(x, c)
    # plotDifferenceOfCorrelations(xP, c, isPileUp = True)

    
    
    '''
    ------------------------------------------------------------
                        Results-related plots
    ------------------------------------------------------------
    '''
    RESULTS_FIXED_SNR_VALUE = 3.0
    RESULTS_FIXED_SNR_VALUE_PR_CURVE = 3.0
    RESULTS_FIXED_THRESHOLD_VALUE = 0.5
    RESULTS_THRESHOLD_LIMITS = (0.1, 1.0)
    # RESULTS_THRESHOLD_LIMITS_PR_CURVE = (0.6, 1.75)
    RESULTS_THRESHOLD_LIMITS_PR_CURVE = (0.15, 1.75)
    RESULTS_SNR_LIMITS = (1, 8)
    RESULTS_SNR_CURVE_FAMILIES = (1, 2, 3, 6)
    RESULTS_THRESHOLD_CURVE_FAMILIES = (0.1, 0.3, 0.6, 0.9)
    
    
    resultsPlots = PlotResults(resultsFile = resultsFile, fontSize = DEFAULT_FONT_SIZE, currentDir = currentDir)
    resultsComputations = ResultsComputations(df = resultsPlots.getDataset())
    resultsHardware = HardwareResults(hlsresultsFile = hwResultsFile, fontSize = DEFAULT_FONT_SIZE)
    
    
    # resultsPlots.plotThresholdSweepFixedSNR_TrigCount(snrValue = RESULTS_FIXED_SNR_VALUE, plotOriginal = True, plotModified = False, plotSignal = True,
    #                                                   thresholdLimits = [0, 1.5])
    # resultsPlots.plotThresholdSweepFixedSNR_CSI(snrValue = RESULTS_FIXED_SNR_VALUE, thresholdLimits = RESULTS_THRESHOLD_LIMITS, interpolate = False)
    resultsPlots.plotSNRSweepFixedThreshold_CSI(thresholdValue = RESULTS_FIXED_THRESHOLD_VALUE, snrLimits = RESULTS_SNR_LIMITS, interpolate = False)
    resultsPlots.plotThresholdSweepSNRFamilyCurves_CSI(snrValues = RESULTS_SNR_CURVE_FAMILIES,
                                                       thresholdLimits = RESULTS_THRESHOLD_LIMITS,
                                                       xLabelPositionLimits = (0.4, 1.03),
                                                       verticalTriggerLine = None)
    
    

    resultsPlots.plotThresholdSweepFixedSNR_PR(snrValue = RESULTS_FIXED_SNR_VALUE_PR_CURVE,
                                            thresholdLimits = RESULTS_THRESHOLD_LIMITS_PR_CURVE,
                                            plotOriginal = True,
                                            plotModified = False,
                                            plotSignal = True,
                                            outputFileName = 'thresholdSweepFixedSNR_PR_signalAndPearson.pdf',
                                            showTitle = False)

    resultsPlots.plotThresholdSweepFixedSNR_PR(snrValue = RESULTS_FIXED_SNR_VALUE_PR_CURVE,
                                            thresholdLimits = RESULTS_THRESHOLD_LIMITS_PR_CURVE,
                                            plotOriginal = True,
                                            plotModified = True,
                                            plotSignal = True,
                                            outputFileName = 'thresholdSweepFixedSNR_PR_signalAndPearsonAndSPCC.pdf',
                                            showTitle = True)

    
    
    resultsPlots.plotThresholdSweepFixedSNR_PR(snrValue = RESULTS_FIXED_SNR_VALUE_PR_CURVE,
                                            thresholdLimits = RESULTS_THRESHOLD_LIMITS_PR_CURVE,
                                            plotOriginal = True,
                                            plotModified = True,
                                            plotSignal = False)
    
    
    x = resultsComputations.computeAUCPR_FixedSNR(snrValue = RESULTS_FIXED_SNR_VALUE_PR_CURVE,
                                        thresholdLimits = RESULTS_THRESHOLD_LIMITS_PR_CURVE,
                                        plotOriginal = True,
                                        plotModified = True,
                                        plotSignal = False)
        
    aucSweep = resultsComputations.computeAUCPR_sweepSNR(plotOriginal = True, plotModified = True,
                                                  plotSignal = True, thresholdLimits = RESULTS_THRESHOLD_LIMITS_PR_CURVE,
                                                  snrLimits = RESULTS_SNR_LIMITS)

    resultsPlots.plotAUCPR_sweep(aucSweep, plotOriginal = True, plotModified = True, plotSignal = False, 
                                 thresholdLimits = RESULTS_THRESHOLD_LIMITS_PR_CURVE, snrLimits = RESULTS_SNR_LIMITS,
                                 interpolate = False)
    
    resultsPlots.plotAUCPR_sweep(aucSweep, plotOriginal = True, plotModified = True, plotSignal = True, 
                                 thresholdLimits = RESULTS_THRESHOLD_LIMITS_PR_CURVE, snrLimits = RESULTS_SNR_LIMITS,
                                 interpolate = False)

    
    resultsHardware.plotClassicSimVsHw()
    resultsHardware.plotClassicDiff()
    resultsHardware.plotModifiedSimVsHw()
    resultsHardware.plotModifiedDiff()
    resultsHardware.plotAll()
    resultsHardware.plotClassicAndModifiedDiff()
    resultsHardware.plotClassicSimVsClassicAndModifiedHw()
    
    
    resultsHardware.stackSubPlots([resultsHardware.plotClassicSimVsHw, 
                                  resultsHardware.plotModifiedSimVsHw])

    resultsHardware.stackSubPlots([resultsHardware.plotClassicSimVsHw,
                                    resultsHardware.plotClassicDiff],
                                    fileName = 'hwResults_stacked_ClassicAndClassicDifference.pdf')
    
    resultsHardware.stackSubPlots([resultsHardware.plotModifiedSimVsHw,
                                    resultsHardware.plotModifiedDiff],
                                    fileName = 'hwResults_stacked_ModifiedAndModifiedDifference.pdf')

    resultsHardware.stackSubPlots([resultsHardware.plotClassicDiff,
                                    resultsHardware.plotModifiedDiff],
                                    fileName = 'hwResults_stacked_BothDifferences.pdf')
    
    resultsHardware.stackSubPlots([resultsHardware.plotClassicSimVsHw, 
                                  resultsHardware.plotModifiedSimVsHw,
                                  resultsHardware.plotClassicAndModifiedDiff],
                                fileName = 'hwResults_stacked_ClassicAndModifiedAndDifferences.pdf',
                                pltRatio = 0.175)

    print("Plotting, done")
    